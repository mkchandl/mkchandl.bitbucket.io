# -*- coding: utf-8 -*-
"""
@file L4shares.py

This file contains the variables that are shared between classes for lab 4

Created on Thu Oct 22 16:02:52 2020

@author: Michelle Chandler
"""

cmd = None


time = None

position = None


import array 
resp_p = array.array('B', [])
resp_t = array.array('B', [])