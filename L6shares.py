# -*- coding: utf-8 -*-
"""
@file L6shares.py

Created on Mon Nov 16 11:43:03 2020

@author: Michelle Chandler
"""

gain = 0

L = None

fromcomp = None

oldposition = None

position = None

delta = None #This is in ticks/interval

delta_rpm = None #This is in rpm

cmd = None

time = None

interval = None

import array 
resp_v = array.array('f', [])
resp_t = array.array('f', [])
resp_L = array.array('f', [])