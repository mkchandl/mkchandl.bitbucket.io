# -*- coding: utf-8 -*-
"""
@file touchpadDriver.py
@brief ME 405 Touchpad Driver
@date Feb 23 2021
@author: Michelle Chandler
"""

import pyb
from pyb import Pin
from pyb import ADC

class touchpadDriver:
    '''
    @brief      This is a driver for the touchpad
    @details    This driver can tell when an object in placed on the touchpad and
                the current location of the object
    '''
    
    def __init__(self, xm, xp, ym, yp, width, length, xc, yc):
        '''
        @brief      touchpad driver constructor
        @param xm   the x minus pin 
        @param xp   the x plus pin
        @param ym   the y minus pin 
        @param yp   the y plus pin
        @param width the width of the touchpad
        @param length the length of the touchpad
        @param xc the origin of the touchpad in the x direction
        @param yc the origin of the touchpad in the y direction
        '''
        
        self.pin_xm = xm
        # self.pin_xm = Pin (Pin.cpu.A1)

        self.pin_xp = xp
        # self.pin_xp = Pin (Pin.cpu.A0)
        
        self.pin_ym = ym
        # self.pin_ym = Pin(Pin.cpu.A7)
        
        self.pin_yp = yp
        # self.pin_yp = Pin(Pin.cpu.A6)
        
        self.width = width
        self.length = length
        self.xc = xc
        self.yc = yc
        
    def getX(self):
        '''
        @brief method to get the x position of the point of contact on the touchpad
        '''
        # initialize xm xp
        self.pin_xm.init(mode=Pin.OUT_PP, value=0) #init as low
        self.pin_xp.init(mode=Pin.OUT_PP, value=1) # init as high
        self.pin_ym.init(mode = Pin.IN) # init as input
        self.pin_yp.init(mode = Pin.IN) # init as input
        self.pin_ym.init(mode=Pin.ANALOG)
        self.ADC_ym = ADC(self.pin_ym)
        
        read_x = (self.length*self.ADC_ym.read()/4095)-self.xc # fractional position
        return(read_x)
    
    def getY(self):
        '''
        @brief method to get the y position of the point of contact on the touchpad
        '''        
        self.pin_ym.init(mode=Pin.OUT_PP, value=0) #init as low
        self.pin_yp.init(mode=Pin.OUT_PP, value=1) # init as high
        self.pin_xm.init(mode = Pin.IN) # init as input
        self.pin_xp.init(mode = Pin.IN) # init as input
        self.pin_xm.init(mode=Pin.ANALOG)
        self.ADC_xm = ADC(self.pin_xm)
        
        read_y = (self.width*self.ADC_xm.read()/4095)-self.yc # fractional y position
        return(read_y)
    
    def getZ(self):
        '''
        @brief method to get the z position of the point of contact on the touchpad
        '''        
        self.ADC_ym = ADC(Pin.cpu.A7)
        self.pin_ym = Pin(Pin.cpu.A7, mode=Pin.IN)
        self.pin_ym.init(mode=Pin.ANALOG)
        self.ADC_ym = ADC(self.pin_ym)
        
        self.ADC_xp = ADC(Pin.cpu.A0)
        
        self.pin_xm.init(mode=Pin.OUT_PP, value=0) #init as low
        self.pin_yp.init(mode=Pin.OUT_PP, value=1)
        
        read_z = self.ADC_ym.read()
        if read_z == 0:
            contact_flag = 0
            return(contact_flag)
        elif read_z >= 4087:
            contact_flag = 0
            return(contact_flag)
        else:
            contact_flag = 1
            return(contact_flag)
            
    
    def getXYZ(self):
        '''
        @brief method to get the x, y, and z position of the point of contact on the touchpad as a tuple
        '''        
        Y = self.getY()
        X = self.getX()
        Z = self.getZ()
        xyz_tuple = [X, Y, Z]
        return(xyz_tuple)
        
    
if __name__ == "__main__":
    # create pin objects
    xm = Pin (Pin.cpu.A1)
    xp = Pin (Pin.cpu.A0)
    ym = Pin(Pin.cpu.A7)
    yp = Pin(Pin.cpu.A6)
    
    #need driver object
    touchpad_driver = touchpadDriver(xm,xp,ym,yp, 99.36, 176.64, 88.32, 49.68)
    
    import utime 
    
    while True:
        print('getting Z position')
        contact_flag = touchpad_driver.getZ()
        if contact_flag == 0:
            print('no contact')
        elif contact_flag == 1:
            print('contact')
            start_time = utime.ticks_us()
            print('position: ' + str(touchpad_driver.getX()))
            end_time = utime.ticks_us()
            print('elapsed time: ' + str(start_time-end_time))
        else:
            pass