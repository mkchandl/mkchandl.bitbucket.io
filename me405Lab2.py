"""
@file me405Lab2.py
@brief This is the main .py for ME405 Lab 2
@date 22 Jan 2021
@author: Michelle Chandler
"""

import pyb
import random
import me405L2shares
import time
import micropython
# import keyboard

# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

rxn_time = []

def pickDelay():
    '''
    @brief a function to pick the random delay for the LED
    '''
    rand_var = random.random()
    delay_time = rand_var + 2
    return delay_time

## Create a timer object (timer 1) in general purpose counting mode
tim = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

def myCallback(which_pin):
    '''
    @brief callback which runs when user presses button
    '''
    # need this to be shares so can use as trigger for change state?
    # set as none in s0
    me405L2shares.stop_count = tim.counter()
    
## Pin object to use for blinking LED. Attached to PA5
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)



## Extint Object 
extint = pyb.ExtInt (pyb.Pin.board.PC13,      # Which pin
             pyb.ExtInt.IRQ_FALLING,      # Interrupt on FALLING edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             myCallback)                  # Interrupt service routine

## set state
state = 0 

while True:
    try:
        if state == 0:
            print('state 0')
            me405L2shares.stop_count = None
            delta = None
            me405L2shares.stop_count = None
            me405L2shares.start = None
            period = 0x7FFFFFFF
            delay_time = pickDelay()
            time.sleep(delay_time)
            print('delay')
            me405L2shares.start = tim.counter()
            state = 1
            
        elif state == 1:
            # print('state 1')
            myPin.high()
            curr_count = tim.counter()
            if curr_count - me405L2shares.start >= 1e6:
                print('over time limit')
                myPin.low()
                state = 0
            elif me405L2shares.stop_count != None:
                print('interrupted')
                myPin.low()
                # get the number of counts from when the LED turns on to when the button is pressed
                delta = me405L2shares.stop_count - me405L2shares.start
                # dealing with overflow 
                if delta > period/2:
                    delta -= period
                elif delta < -period/2:
                    delta += period
                # unit conversion from counter to time
                delta = delta*period
                rxn_time.append(delta)
                me405L2shares.rxn_time = rxn_time
                state = 0
        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
    except KeyboardInterrupt:
        print('Average Reaction Time: '  + str(sum(rxn_time)/len(rxn_time)/16))
        break
        
           
           
        