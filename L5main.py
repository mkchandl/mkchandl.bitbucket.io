# -*- coding: utf-8 -*-
"""
@file L5main.py

Created on Mon Nov  9 13:00:47 2020

@author: CCHAN
"""

from pyb import UART
import L5shares

class BLE_Driver:
    '''
    @brief      A Class that drives bluetooth communication and LED state
    @details    The BLE Driver class takes input over bluetooth from a phone app, 
                saves the data to be used by the BLE task, and sends a message 
                back to the phone app
    '''
    
    def __init__(self, pinA5):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA6    pin on nucleo
        @param pinA7    pin on nucleo
        '''
        
        ## Pin object used for Channel 1
        self.pinA5 = pinA5

        ## Uart communication 
        self.myuart = UART(3, baudrate = 9600)
        
    def CheckInput(self):  
        '''
        @brief checks for character from phone, saves character, send message to phone
        '''
        
        if self.myuart.any():
            L5shares.cmd = None
            L5shares.cmd = self.myuart.readchar()
            # Writing back to phone
            L5shares.resp.append(L5shares.cmd)
            for n in range(len(L5shares.resp)):
                    self.myuart.write('{:}\r\n'.format(L5shares.resp[n]))
        
        else:
            L5shares.cmd = None
            
    def CheckAny(self):
        ''' 
        @brief THis method checks if there are any characters to ve read
        '''
        L5shares.check_any = None
        # variable containing the number of characters left to be read 
        L5shares.check_any = self.myuart.any()
        
    def ReadAny(self):
        ''' 
        @brief THis method reads any characters that need to be read
        ''' 
        L5shares.cmd = None
        L5shares.cmd = self.myuart.readchar()
        # Writing back to phone
        L5shares.resp = ['LED blinking in Hz at']
        L5shares.resp.append(L5shares.cmd)
        for n in range(len(L5shares.resp)):
                    self.myuart.write('{:}\r\n'.format(L5shares.resp[n]))
        
    def LEDsame_state(self): 
        '''
        @brief This method does not change the state of the LED
        '''
        # If the LED is on it stays on
        if self.pinA5.value()== 1:
            self.pinA5.high()
        # If the lED is off it stays off    
        elif self.pinA5.value()== 0:
            self.pinA5.low()
        
    def LEDchange_state(self):
        '''
        @brief This method changes the state of the LED
        '''
        # If the LED is on it turns off
        if self.pinA5.value()== 1:
            self.pinA5.low()
        # If the lED is off it turns on   
        elif self.pinA5.value()== 0:
            self.pinA5.high()


import utime
import pyb

class BLE_Task:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_WAIT_FOR_CMD     = 1    
    
    ## Constant defining State 2
    S2_BLINK_AT_FREQUENCY      = 2    
    
    ## Constant defining State 5
    S3_DO_NOTHING  = 3
    
    def __init__(self, interval, BLE_Driver_object):
        '''
        @brief            Creates a TaskWindshield object.
        @param LED        An object from class PowerSupply representing the LED
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The power supply object that is on or off
        self.BLE_Driver_object = BLE_Driver_object
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Counter for state 2. Is re-zeroed at the end of state 1
        self.count = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        

        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        #self.buttonprepare()
        #self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        #self.tim2 = pyb.Timer(2, freq = 20000)
        #self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
       
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('state 0')
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code 
                print('state 1')
                self.BLE_Driver_object.CheckInput()
                if L5shares.cmd != None:
                    print('cmd value found going to s2')
                    self.transitionTo(self.S2_BLINK_AT_FREQUENCY)
                    # Initialize the frequency value
                    self.frequ = L5shares.cmd
                    self.count = 0
                else:
                    print('no cmd val found')
                    self.transitionTo(self.S1_WAIT_FOR_CMD) 
            
            elif(self.state == self.S2_BLINK_AT_FREQUENCY):
                print('State 2')
                # Run State 2 Code
                self.BLE_Driver_object.CheckAny()
                print('check any = ' + str(L5shares.check_any))
                if L5shares.check_any != 0:
                    print('reading any characters')
                    self.BLE_Driver_object.ReadAny()
                    print('saving new frequency')
                    self.frequ = L5shares.cmd
                else:
                    # val is a variable that represents the seconds the light 
                    # remains at one state, in seconds.
                    print('no new input, blinking at freq'+ str(L5shares.cmd))
                    self.val = 1e6/self.frequ
                    print('val = ' + str(self.val))
                    print('self.count = ' + str(self.count))
                    # these conditionals determine whether or not the LED remains
                    # in its current state or changes state
                    if (self.count < self.val):
                        self.BLE_Driver_object.LEDsame_state()
                        print('LED same state') 
                    elif (self.count > self.val):
                        self.BLE_Driver_object.LEDchange_state()
                        self.count = 0
                        print('LED change state')
                
            elif(self.state == self.S3_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
            # Specifying the next cound
            self.count = utime.ticks_add(self.interval, self.count)
            # print(str(self.count))
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        

pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) 

BLE_Driver_object = BLE_Driver(pinA5)

BLE_Task = BLE_Task(.01, BLE_Driver_object)

while True:
   BLE_Task.run() 