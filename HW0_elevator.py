'''
@file HW0_elevator.py

This file contains the FSM elevator task. 

The user has a button to turn on or off the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator going up and down.
    @details    This class implements a finite state machine to control the 
                operation of a virtual elevator between 2 floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP        = 2    
    
    ## Constant defining State 3
    S3_STOPPED_AT_1     = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_2     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def __init__(self, interval, Floor1Sensor, Floor2Sensor, Floor1Button, Floor2Button, Motor):
        '''
        @brief            Creates a TaskWindshield object.
        @param Floor1Sensor   An object from class Button representing a sensor on the first floor
        @param Floor2Sensor  An object from class Button representing a sensor on the second floor
        @param Floor1Button An object from class Button representing the button to take the elevator to the first floor
        @param Floor2Button An object from class Button representing the button to take the elevator to the second floor
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the bottom limit
        self.Floor1Sensor = Floor1Sensor
        
        ## The button object used for the top limit
        self.Floor2Sensor = Floor2Sensor
        
        ## The button object used for the floor 1 choice
        self.Floor1Button = Floor1Button
        
         ## The button object used for the floor 2 choice
        self.Floor2Button = Floor2Button
        
        ## The motor object raising/ loweering the elevator
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.Floor1Sensor.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_AT_1)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                if(self.Floor2Sensor.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_AT_2)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_AT_1):
                # Transition to state 1 if the left limit switch is active
                if(self.Floor2Button.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_AT_2):
                # Run State 4 Code
                if(self.Floor1Button.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents some buttons that include the sensors used 
                to trigger change of states as well as push buttons users push to 
                go from one floor to another.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator 
                up, down, and stop.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor forward/ up
        '''
        print('Motor moving up, (CCW)')
    
    def Down(self):
        '''
        @brief Moves the motor reverse/ down
        '''
        print('Motor moving down, (CW)')
    
    def Stop(self):
        '''
        @brief Stops Motor
        '''
        print('Motor Stopped')



























