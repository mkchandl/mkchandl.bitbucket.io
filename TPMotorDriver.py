# -*- coding: utf-8 -*-
'''
@file       TPMotorDriver.py
@brief      Contains MotorDriver class for PWM motor control
@details    Used for motor control with Nucleo L476RG board along with H-Bridge 
            chip.

@author     Michelle Chandler
@author     Ben Sahlin
@date       March 8, 2020
'''

import pyb
import micropython

class MotorDriver:
    '''
    @brief      This class implements a motor driver for the ME 305 board
    @details    This class controls a DC motor using pulse width modulation. 
                Designed to work with Nucleo L476RG board along with an H-Bridge 
                chip.               
    '''
    
    def __init__ (self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN2_pin, IN3_pin, IN4_pin, timer):
            ''' 
            @brief              Creates motor driver object
            @details            Creates a motor driver by initializing GPIO
                                pins and turning the motor off for safety.
            @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
            @param nFAULT_pin   A pyb.Pin object to use as the fault detection pin
            @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
            @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
            
            
            @param timer        A pyb.Timer object to use for PWM generation on
                                IN1_pin and IN2_pin.
            '''
            self.debug = False
            self.nSLEEP_pin = nSLEEP_pin
            self.nFAULT_pin = nFAULT_pin
            #self.nFAULT_pin.high()
            self.IN1_pin = IN1_pin
            self.IN2_pin = IN2_pin
            self.IN3_pin = IN3_pin
            self.IN4_pin = IN4_pin
            self.timer = timer
            
            #motor 1
            self.tch1 = self.timer.channel(self.mapPinChannel(self.IN1_pin), pyb.Timer.PWM, pin=self.IN1_pin)
            self.tch2 = self.timer.channel(self.mapPinChannel(self.IN2_pin), pyb.Timer.PWM, pin=self.IN2_pin)
            self.nSLEEP_pin.low()               #start with motor disabled
            self.tch1.pulse_width_percent(0)    #start with motor off
            self.tch2.pulse_width_percent(0)    #start with motor off
            
            #motor 2
            self.tch3 = self.timer.channel(self.mapPinChannel(self.IN3_pin), pyb.Timer.PWM, pin=self.IN3_pin)
            self.tch4 = self.timer.channel(self.mapPinChannel(self.IN4_pin), pyb.Timer.PWM, pin=self.IN4_pin)            
            self.tch3.pulse_width_percent(0)    #start with motor off
            self.tch4.pulse_width_percent(0)    #start with motor off
            
            #set up global vars for fault detection
            # global nfault_flag
            # nfault_flag = 0
            
            #create buffer for interrupt errors
            micropython.alloc_emergency_exception_buf(100)
            
            #set up nFAULT interrupt, expect error here on second motor object initialization
            try:
                self.extint = pyb.ExtInt(self.nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.nFAULTCallback)
 #               self.extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.nFAULTCallback)
                
            except ValueError:
                #this happens on second motor object creation on the same H-Bridge
                #nFAULT interrupt is already created, so do nothing this time
                pass
            #print('obj init pin nfault value: {:}'.format(self.nFAULT_pin.value()))
            #print('after extint creation nfault value: {:}'.format(self.nFAULT_pin.value()))            
            self.fault_detected = False #create fault detection boolean for interrupt method
            print('Motor driver object created on pins: \r\n{:} \r\n{:}'.format(self.IN1_pin, self.IN2_pin))
            
            
    def enable(self):
        '''
        @brief      Enables motor control
        @details    Turns on motor control by setting nSLEEP pin for H-Bridge 
                    chip to high. If a fault was detected it also power cycles 
                    the H-Bridge chip and then resets the software fault detection 
                    to False.
        '''
        try:
            self.extint.disable()
        except AttributeError:
            #happens when using second motor object
            pass
        if self.fault_detected == True:
            # resuming motor operation after fault detected
            # refer to DRV8847 datasheet sect 7.3.8.4
            # power cycling is done by sleep.low()/sleep.high()
            self.nSLEEP_pin.low()
            self.nSLEEP_pin.high()            
            self.fault_detected = False
            print('Re-enabling Motor. Fault detection reset')
        else:
            # enabling motor not from fault
            self.nSLEEP_pin.high()
            print('Enabling Motor')
        try:
            self.extint.enable()
        except AttributeError:
            #happens when using second motor object
            pass
        
    def disable(self):
        '''
        @brief      Disables motor control
        @details    Turns off motor control by setting nSLEEP pin for H-Bridge 
                    chip to low.
        '''
        #turn off sleep pin
        self.nSLEEP_pin.low()
        print('Disabling Motor')
                
    def setDuty1(self, duty1):
        '''
        @brief          Sets motor 1 duty cycle.
        @details        Sets motor speed using pulse width modulation (PWM). Uses 
                        PWM scheme that pulses one pin or the other depending on 
                        desired direction.
        @param duty1    Integer from -100 to 100 representing desired 
                        duty1 for the pulse width modulation cycle. Negative 
                        values cause the motor to spin the opposite way.
        '''        
        if self.fault_detected == True:
            #motor in fault state
            print('Fault detected. Use MotorDriver.enable() to re-enable operation')
        elif self.fault_detected == False:
            self.duty1 = duty1
            #don't allow higher inputs than abs(100)
            if self.duty1 >= 100:
                self.duty1 = 100
            elif self.duty1 <= -100:
                self.duty1 = -100
            #set duty
            if self.duty1 >= 0:
                #cw
                self.tch1.pulse_width_percent(abs(self.duty1))
                self.tch2.pulse_width_percent(0)
            elif self.duty1 < 0:
                #ccw
                self.tch1.pulse_width_percent(0)
                self.tch2.pulse_width_percent(self.duty1)
            self.debugprint('Motor: {:} Spinning at duty {:}'.format(self, self.duty1))
        else:
            print('Invalid motor driver fault state')

    def setDuty2(self, duty2):
        '''
        @brief          Sets motor 2 duty cycles.
        @details        Sets motor speed using pulse width modulation (PWM). Uses 
                        PWM scheme that pulses one pin or the other depending on 
                        desired direction.
        @param duty2     Integer from -100 to 100 representing desired 
                        duty for the pulse width modulation cycle. Negative 
                        values cause the motor to spin the opposite way.
        '''        
        if self.fault_detected == True:
            #motor in fault state
            print('Fault detected. Use MotorDriver.enable() to re-enable operation')
        elif self.fault_detected == False:
            self.duty2 = duty2
            #don't allow higher inputs than abs(100)
            if self.duty2 >= 100:
                self.duty2 = 100
            elif self.duty2 <= -100:
                self.duty2 = -100
            #set duty
            if self.duty2 >= 0:
                #cw
                self.tch3.pulse_width_percent(abs(self.duty2))
                self.tch4.pulse_width_percent(0)
            elif self.duty2 < 0:
                #ccw
                self.tch3.pulse_width_percent(0)
                self.tch4.pulse_width_percent(self.duty2)
            self.debugprint('Motor: {:} Spinning at duty {:}'.format(self, self.duty2))
        else:
            print('Invalid motor driver fault state')
            
    def mapPinChannel(self, pin):
        '''
        @brief      Returns channel for specified pin
        @details    Designed with Nucleo L476RG board.
        @param pin  Pin object to map to corresponding channel
        '''
        if pin == pyb.Pin.cpu.B4:
            #Pin B4 corresponds to Timer 3 Channel 1
            return 1
        elif pin == pyb.Pin.cpu.B5:
            #Pin B5 corresponds to Timer 3 Channel 2
            return 2
        elif pin == pyb.Pin.cpu.B0:
            #Pin B0 corresponds to Timer 3 Channel 3
            return 3
        elif pin == pyb.Pin.cpu.B1:
            #Pin B1 corresponds to Timer 3 Channel 4
            return 4
        else:
            print('Unknown channel for specified pin {:}'.format(pin))
            
    def nFAULTCallback(self, pin):
        '''
        @brief      Callback function for fault detection pin
        @details    Triggers when nFAULT pin is pulled low based on H-Bridge 
                    fault detection. Turns off motor and sets software fault 
                    detection to True.
        '''
        #from testing, pin=2           
        print('Motor fault detected. Use MotorObject.enable() to re-enable motor control')
        #disable motor obj
        self.disable()                #sets nSLEEP to low
        self.fault_detected = True    #sets software fault detection
        
        
    #  Michelle nfault callback function. the power cycling is done by sleep.low()/sleep.high()
    #  refer to DRV8847 datasheet sect 7.3.8.4
    # nfault flag


    # def myCallback(which_pin):
    #     '''
    #     @brief callback which runs when user presses button
    #     '''
    #     # need this to be shares so can use as trigger for change state?
    #     # set as none in s0 
    #     print('nFAULT triggered')
    #     nSLEEP_pin.low()
    #     nSLEEP_pin.high()
    #     nfault_flag = 1
    ##Ben - I think that we don't need a shares file if we use the self param in the cb function so that you can set self.fault_detected = True
    ## reference for using self in cb function: https://docs.micropython.org/en/latest/reference/isr_rules.html
    
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
        
if __name__ == '__main__':
    #for testing purposes, if this file is run by itself it will run this code
    #test with motor 1: Timer 3 channels 1 and 2, pins B4 and B5
    nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    nFAULT_pin = pyb.Pin(pyb.Pin.cpu.B2)#, pyb.Pin.IN, pyb.Pin.PULL_UP)
    #print('pin creation nfault value: {:}'.format(nFAULT_pin.value()))

    
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    
    tim = pyb.Timer(3, freq=20000)
    
    moe = MotorDriver(nSLEEP_pin, nFAULT_pin, pin_IN1, pin_IN2, pin_IN3, pin_IN4, tim)
    #moe2 = MotorDriver(nSLEEP_pin, nFAULT_pin, pin_IN3, pin_IN4, tim)
    moe.enable()
    while True:
        moe.setDuty1(40)
        moe.setDuty2(40)
    


