# -*- coding: utf-8 -*-
"""
@file HW01.py
@author Michelle Chandler
@brief This is the .py for hw 01. 
@details This .py returns change in the least amount of coins.
@date Created Jan 8 2021

"""


def getChange(price, payment):
    '''
    @brief A function that calculates the change required in the least number of coins
    @param price an integer representing the price of an object in cents
    @param payment a tuple representing the amount paid and what types of coins/dollars were used
    @return if funds are sufficient returns tuple of change if funds are insufficient
            returns none
    '''
    final_change = 8*[0]
    check_payment = 1*payment[0]+5*payment[1]+10*payment[2]+25*payment[3]+100*payment[4]+100*5*payment[5] + 1000*payment[6] + 100*20*payment[7]
    print('check_payment: ' + str(check_payment))
    check_price = price*100 #cents
    print('check_price: ' + str(check_price))
    if price <= 0:
        print('please input a positive number')
    if check_payment < check_price:
        print('insufficient funds')
    elif check_payment == check_price:
        print('exact payment, no change required')
    else:
        extra_change = check_payment - check_price #cents
        # check units: what is in cents and what is in dollars. keep all in cents
        change = extra_change #cents
        
        twenties = change/(20*100)
        if twenties > 1:
            twenties = int(twenties)
            final_change[7] = twenties
            change = change - twenties*20*100
        else:
            pass
            
        tens = change/(10*100)
        if tens > 1:
            tens = int(tens)
            final_change[6] = tens
            change = change - tens*10*100
        else:
            pass
        
        fives = change/(5*100)
        if fives > 1: 
            fives = int(fives)
            final_change[5] = fives
            change = change- fives*5*100
        else:
            pass
        
        ones = change /(1*100)
        if ones > 1:
            ones = int(ones)
            final_change[4] = ones
            change = change - ones*100
        else:
            pass
        
        quarters = change/(25)
        if quarters > 1:
            quarters = int(quarters)
            final_change[3] = quarters
            change = change - quarters*25
        else:
            pass
        
        dimes = change/10
        if dimes > 1:
            dimes = int(dimes)
            final_change[2] = dimes
            change = change - dimes*10
        else:
            pass
            
        nickles = change/5
        if nickles > 1:
            nickles = int(nickles)
            final_change[1] = nickles
            change = change - nickles*5
        else:
            pass

        pennies = change/1
        if pennies > 1:
            final_change[0] = pennies
        else:
            pass
            
        print('returned change: ' + str(final_change))
        
if __name__ == "__main__":
    my_payment = (3,0,0,0,1,0,0,1)
    my_price = 15.5
    getChange(my_price, my_payment)