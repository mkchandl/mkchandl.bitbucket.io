# -*- coding: utf-8 -*-
"""
@file me405lab4_main.py
@brief ME 405 lab 4 main.py. Contains while loop to collect temperature data
@date Created on Tue Feb  2 14:26:16 2021
@author: Michelle Chandler, Hunter Brooks
"""

# ampy --port COM5 get me405lab4.csv me405lab4.cs


import pyb
from pyb import I2C
from mcp9808 import mcp9808
import utime
import array

amb_temp = array.array('f', [])
mcu_temp = array.array('f', [])
time     = array.array('f', [])

## Master i2c object
print('i2c object')
i2c = I2C(1, I2C.MASTER)
# adcall object
print('adcall object')
adcall = pyb.ADCAll(12, 0x70000)

# mcp object for mcp 9808 driver
mcp9808 = mcp9808(i2c)

# to collect time for graphs
time_to_measure = utime.ticks_ms()    # 8 hours is 28800s or 28800000ms
current_time = 0
time_stamp= 0
exit_program = 0
current_time = utime.ticks_ms()


while True: 
    # check function scans for i2c devices on bus and returns address, checking it against the input address
    check = mcp9808.check(24)
    # if the found address matches the input, the check flag is set high
    if check == 1:
        try:
            if (utime.ticks_diff(current_time, time_to_measure) >= 0):
                print('reading core temp')
                adcall.read_vref() # without this command the adcall object reutnrs weird core temps
                mcu_temp_var = adcall.read_core_temp() # reading the mcu core temperature
                mcu_temp.append(mcu_temp_var) # save mcu temperature to array
                print('MCU Temp' + str(mcu_temp))
                amb_temp_var = mcp9808.celsius() # read the ambient temperature
                amb_temp.append(amb_temp_var) # save the ambient temp reading to array
                print('Temp Sensor, C' + str(amb_temp))
                current_time = current_time + 60  # current time in seconds
                time.append(current_time)
                mcu_temp_var = None
                amb_temp_var = None
                utime.sleep_ms(60000)
            else:
                pass
            current_time = utime.ticks_ms()
    
        except KeyboardInterrupt: 
            # these lines enough to open adn close file
            with open ("me405lab4_2.csv", "w") as me405lab4:
                # how much will write into file
                # for line in range (10) 
                me405lab4.write ('Time [s]: , Amb_temp: , mcu_temp: \r\n')
                for n in range(len(mcu_temp)):
                    # what to write into file
                    me405lab4.write ('{:}, {:}, {:}\r\n'.format(time[n], amb_temp[n], mcu_temp[n]))
                    # ... 
            print ("The file has by now automatically been closed.")
            break 
    elif check == 0:
        break 

