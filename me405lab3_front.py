"""
@file me405lab3_front.py
@brief me405 lab 3 spyder front end
@date Jan 26 2021
@author: Michelle Chandler
"""

import serial 
import numpy as np
import matplotlib.pyplot as plt
import time

## serial port
ser = serial.Serial(port= 'COM5', baudrate=115273, timeout=1)


n=0
while(n==0):
    inv = input('Enter G or g: ')
    if(inv=='G' or inv=='g'):
        ser.write(str(inv).encode('ascii'))
        n = 1
    else:
        print('Please input G or g')

voltage = []
number = []
        
# wait for response
buf_string = ser.readline().decode('ascii')
# pause on reading for characters to give the nucleo time to collect data and send 
# back to pc
time.sleep(12)
# while there are characters in repl to be read
while ser.in_waiting != 0 : 
    buf_string = ser.readline().decode('ascii')
    buf_list = buf_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears;
    voltage.append(int(buf_list[1]))  
    number.append(int(buf_list[0]))
    if len(voltage) >= 999:
        # there's a weird glitch where the computer reads a >>> after the buffer
        # is finished being sent over. This is just a way to strong arm it into 
        # ignoring the >>> and still plotting the data points.
        ## Plotting Data from Encoder        
        fig, ax = plt.subplots()  # Create a figure containing a single axes.
        ax.plot(number, voltage, 'g--')  # Plot some data on the axes. 
        ax.set_xlabel('Reading Number')
        ax.set_ylabel('Count')
        ax.set_title("ME405 Button Voltage Over Time")
                            
        ## Saving the two nicropy arrays as a 2d numpy array
        arr = np.array([number, voltage])
        ## Using the numpy array to save the data as a CSV file
        np.savetxt('buttonvoltage.csv', arr, delimiter = ',', fmt = '%d', header = 'Button Voltage versus time')
               

