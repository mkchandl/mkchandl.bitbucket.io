# -*- coding: utf-8 -*-
"""
@file UIfront_end.py

Front end FSM used in lab 6
Created on Mon Oct 26 13:58:29 2020

@author: Michelle Chandler
"""

import L6shares
import time
import serial 
import matplotlib.pyplot as plt



class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Each character received is checked and if it is an english
    letter a-z it is sent through a cipher in TaskCipher.
    
    @image html fsm_task_01.png width=1200px
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_SEND_GAIN    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2
    
    ## Waiting state
    S3_PLOT             = 3
    
    ## Waiting state
    S4_NULL             = 4

    def __init__(self, taskNum, interval):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = interval
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval

        ## Serial port
        self.ser = serial.Serial(port= 'COM3', baudrate=115273, timeout=3)
                
        self.velocity = []
        self.time_= []
        

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.time()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('state 0')
                self.transitionTo(self.S1_SEND_GAIN)
            
            elif(self.state == self.S1_SEND_GAIN):
                print('state 1')
                # Run State 1 Code
                self.gain = input('Provide a gain on the order of .1: ')
                self.gain = float(self.gain)
                # self.gain = int(self.gain*1e1)
                self.ser.write('{:}\r\n'.format(self.gain).encode())
                # self.ser.write(str(self.gain).encode())
                print('sent gain, entering sleep for 10 seconds')
                time.sleep(10)
                self.transitionTo(self.S2_WAIT_FOR_RESP)
                
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                print('state 2')
                self.line_string = self.ser.readline().decode('ascii')
                print('recieved array line')
                print('in waiting  ' + str(self.ser.in_waiting))
                while self.ser.in_waiting !=0 :     
                    # Dealing with stored velocity values
                    self.line_string = self.ser.readline().decode('ascii')
                    print('line string: ' + str(self.line_string))
                    self.line_list = self.line_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
                    print('line list: ' + str(self.line_list))
                    self.velocity.append(int(float(self.line_list[1])))
                    self.time_.append(int(float(self.line_list[0])))
                    if len(self.velocity) >= 198:
                        self.transitionTo(self.S3_PLOT)
                    
            elif(self.state == self.S3_PLOT):
                self.transitionTo(self.S4_NULL)
                ## reset shared variables
                L6shares.resp_v = None    
                L6shares.resp_t = None  
                ## Plotting Data from Encoder 
                self.velocity[0] = 0
                fig, ax = plt.subplots()  # Create a figure containing a single axes.
                ax.plot(self.time_, self.velocity)  # Plot some data on the axes. 
                ax.set_xlabel('Time, t [us]')
                ax.set_ylabel('Velocity [radians/second]')
                ax.set_title("Lab 4 Motor Velocity over time")
                print('Press stop to get plot')
                
            elif(self.state == self.S4_NULL):
                self.transitionTo(self.S4_NULL)
                
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

UI = TaskUser(0, 1)

while True:
    UI.run()