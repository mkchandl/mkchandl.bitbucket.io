"""
@file Lab3.py

This file contains the encoder task/ encoder controller finite state machine

Created on Tue Oct 13 13:12:25 2020

@author: Michelle Chandler
"""
import pyb 
import Lab3shares
import utime
#import EncoderDriver
#import Lab3Main

class Encoder_Task:
    '''
    @brief    Task for the encoder     
    @details  task representing the encoder which is driven by the class encoder
              driver
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Constant defining State 1
    S1          = 1    
    
    def __init__(self, interval, Driver): 
        '''
        @brief      
        '''  
        
        ## The encoder driver object that updates the position of the encoder
        self.Driver = Driver
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
         
        
   
    def run(self):
        '''
        @brief runs one iteration of the task
        '''   
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1)
            
            elif(self.state == self.S1):
                # Run State 1 Code
                # calls the update method in the encoder driver to update the position
                self.Driver.update()
                if Lab3shares.cmd:
                    # if there is a user input, the method handle_input figures out what to do with it
                    Lab3shares.resp = self.handle_input(Lab3shares.cmd)
                    # reset the variable that holds user input
                    Lab3shares.cmd = None
                    
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        '''

        self.state = newState   
           
    def handle_input(self, cmd):
        '''
        @brief      this method deals with user input
        @details    This method looks for specific user input values d, z, p
                    and calls the appropriate methods from the encoder driver 
                    for each given command
        @param cmd  the vaiable that stores the user input
        '''
        
        if cmd ==112:
            # if the user inputs a z into the REPL the set_position() method is called
            self.Driver.set_position()
        
        elif cmd == 80:
            # if the user inputs a p into the REPL the get_position() method is called
            self.Driver.get_position()
            
        elif cmd == 68:
            # if the user inputs a d into the REPL the get_delta() method is called
            self.Driver.get_delta()
        
        else:
            # error handling
            pass
            