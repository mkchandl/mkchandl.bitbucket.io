# -*- coding: utf-8 -*-
"""
@file L7shares.py

This is the shares file for lab7. Like lab6, lab 7 shouldn't really need a shares
file as there is only one task running on the back end but the variables are littered 
within the task and its more work to change them all than its worth.
Created on Dec 1 2020

@author: Michelle Chandler
"""

gain = 0

L = None

fromcomp = None

oldposition = None

position = None

delta = None #This is in ticks/interval

delta_rpm = None #This is in rpm

cmd = None

time = None

interval = None

import array 
resp_v = array.array('f', [])
resp_t = array.array('f', [])
resp_p = array.array('f', [])