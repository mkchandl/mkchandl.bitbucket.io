

'''
@file mainpage.py

@mainpage

@section sec_intro_code Introduction
This is Michelle Chandler's documenation portfolio. 

@section sec_find Finding 305/405 Documentation
The links below take you to ME 305 and ME 405 pages
*  ME 305 (\ref pg_me305)
*  ME 405 (\ref pg_me405)
*  ME 405 Term Project (\ref pg_me405_tp)

@page pg_me305 ME305 Code
This section contains 305 code

@section sec_Fibonacci_code Fibonacci
This lab was designed to familiarize myself with python. In this lab, I wrote a 
script that took in user input in the form of an integer and found the number 
of the fibonacci sequence
that corresponded with the given user index
This lab was focused on: creating functions, taking in user inputs, and basic error handling.
This lab had one .py file. The documenation for that file can be found here:
    https://mkchandl.bitbucket.io/_m_e350_lab01_8py.html
The link for the fibonacci code is below
https://bitbucket.org/mkchandl/me305_labs/src/master/Lab1/ME350Lab01.py

@section sec_Multitask Multitasking
This hw assignment was focused on the basics of finite state machines and classes. In this 
lab I wrote a script to similate an elevator going between two floors. This was done
using various functions and classes including: an elevator task, and a button class, 
This lab focused on generating a better understanding of using classes to create tasks,
designing and coding a finite state machine, and creating various methods within classes.
This lab contains one .py file. The documentaion for that file is here:
    https://mkchandl.bitbucket.io/_h_w0__elevator_8py.html
The link to the elevator source code is below:
https://bitbucket.org/mkchandl/me305_labs/src/master/HW0/
The Finite State Machine diagram for the elevator is below
@image html elevatorFSM.jpg

@section sec_Lab2_code Lab 2 LED
This lab was focused on futhering our understand of classes, tasks, and finite state machines
while also familiarizing ourlselves with basic hardware/ micropython code. This 
lab was also concerned with multitasking, as the written task and classes had to 
run cooperatively. 
This lab has 2 .py files. The documentation for those file can be found here:
    fading LED: https://mkchandl.bitbucket.io/_t1_fading_l_e_dmicropy_8py.html
    Blinking LED: https://mkchandl.bitbucket.io/_t1_virtual_l_e_dmicro_8py.html
The link to the 2 LED source codes are below:
https://bitbucket.org/mkchandl/me305_labs/src/master/Lab2/T1FadingLEDmicropy.py
https://bitbucket.org/mkchandl/me305_labs/src/master/Lab2/T1VirtualLEDmicro.py
The finite state machine diagram for the Fading LED is below
@image html FadingLED.jpg
The finite state machine diagram for the virtual LED is below
@image html VirtualLED.jpg

@section sec_Lab3_code Lab 3 Motor Encoder
This lab was focused on writing an encoder driver class and 
an encoder task to run the driver. This lab emphasized the modular
nature of the encoder driver so that it could be used for multiple 
tasks. This was also the first lab to incorporate a user interface,
where users could input commands in the form of letters: z, p, d, to 
control the encoder position. This lab also relied on lecture topics
in dealing with bad deltas and overflow/underflow
This lab had 5 corresponding .py files. The documentation for these files can be 
found here: 
    User Interface: https://mkchandl.bitbucket.io/_lab3_user_8py.html
    Lab 3 main: https://mkchandl.bitbucket.io/_lab3_main_8py.html
    Encoder Task: https://mkchandl.bitbucket.io/_lab3_8py.html
    Lab 3 shares: https://mkchandl.bitbucket.io/_lab3shares_8py.html
The link to lab 3 source code is here: 
https://bitbucket.org/mkchandl/me305_labs/src/master/Lab3/

@section sec_Lab4_code Lab 4 User Interface
Lab four was really concered with extending the user interface made in lab 3.
While lab 3 the communication was one-directional, this lab utilized bi-directional communication
to send data to and from the computer from the nucleo. 
This lab had 3 .py files associated. The documentation for these files can be found here:
    Front end User Interface: https://mkchandl.bitbucket.io/_u_ifront_8py.html
    Lab 4 main: https://mkchandl.bitbucket.io/main_8py.html
    Lab 4 shares: https://mkchandl.bitbucket.io/_l4shares_8py.html
The link to lab 4 source code is here:
    https://bitbucket.org/mkchandl/me305_labs/src/master/Lab4/
The finite state machine diagram and the task diagram are here:
@image html fsm_task.jpg

@section sec_lab5_code Lab 5 Bluetooth
Lab 5 was focused on using a smart device to create a user interface. This lab relied
on bluetooth rather than serial communication. We edited an app in thunkable to send and recieve
data to and from the nucleo.
This lab had 4 associated .py files. the documenation for these files are here:
    BLE Driver: https://mkchandl.bitbucket.io/_b_l_e___driver_8py.html
    Lab 5 main: https://mkchandl.bitbucket.io/_l5main_8py.html
    Lab 5 shares: https://mkchandl.bitbucket.io/_l5shares_8py.html
    BLE Task: https://mkchandl.bitbucket.io/_b_l_e___task_8py.html
The link to the Lab 5 source code is here:
    https://bitbucket.org/mkchandl/me305_labs/src/master/Lab5/
The link to the Lab 5 thunkable code is here:
    https://x.thunkable.com/projects/5fa5a32625f09500189390ed/d602917c-a412-41fd-95c9-2ff5c2700f34/blocks
The finite state machine diagram for the BLE task is here:
@image html L5FSM.jpg

@section sec_Lab6_code Lab 6 Motor Controller
This lab was concerned with creating a motor driver to control the speed of a 
DC motor, and then creating a closed loop class and controller 
task to perform closed loop speed control on the DC motor. The feedback controller
used was a basic proportional controller.
This lab had 7 associated .py files. The documentation for these files are here:
    Closed Loop Class: https://mkchandl.bitbucket.io/_closed_loop_8py.html
    Backend Controller Task: https://mkchandl.bitbucket.io/_data_gen__task_8py.html
    Encoder Driver: https://mkchandl.bitbucket.io/_l6_encoder_driver_8py.html
    Lab 6 shares: https://mkchandl.bitbucket.io/_l6shares_8py.html
    Motor Driver: https://mkchandl.bitbucket.io/_motor_driver_8py.html
    Frontend User Interface: https://mkchandl.bitbucket.io/_u_ifront__end_8py.html
    Lab 6 main: https://mkchandl.bitbucket.io/_l6main_8py.html
The link to lab 6 source code is here:
    https://bitbucket.org/mkchandl/me305_labs/src/master/Lab6/
The finite state machine diagram for the back end is here:
@image html L6FSM.jpg
The finite state machine diagram for the front end is here:
@image html L6front.jpg
The task diagram is here:
@image html L6task.jpg
The plot for a gain of .25 is here. Note that the omega seems to oscillate around 0
This is because I provided an incorrect ratio of omega_ref to gain resulting in 
little to no motion:
@image html L6plot_25.jpg
The plot for a gain of .36 is here:
@image html L6plot_36.jpg
The plot for a gain of .39 is here:
@image html L6plot_39.jpg

@section sec_Lab7_code Lab 7 Reference Tracking
Lab 7 took lab 6's speed controller and rather than setpoint control
which was lab 6, performed reference tracking. A provided velocity profile 
was used for this lab.
This lab had 7 associated .py files. The documentation for these files are here:
    Closed Loop Class: https://mkchandl.bitbucket.io/_closed_loop_8py.html
    Backend Controller Task: https://mkchandl.bitbucket.io/_l7_data_gen__task_8py.html
    Encoder Driver: Phttps://mkchandl.bitbucket.io/_l7_encoder_driver_8py.html
    Frontend User Interface: https://mkchandl.bitbucket.io/_l7front__end_8py.html
    Lab 7 main: https://mkchandl.bitbucket.io/_l7main_8py.html
    Lab 7 shares: https://mkchandl.bitbucket.io/_l7shares_8py.html
    Motor Driverhttps://mkchandl.bitbucket.io/_motor_driver_8py.html
The link to lab 7 source code is here:
    https://bitbucket.org/mkchandl/me305_labs/src/master/Lab7/
The finite state machine diagram for the back end is here:
@image html L7back_end.jpg
The finite state machine diagram for the front end is here:
@image html L7front_end.jpg
The task diagram is here:
@image html L7task.jpg

@page pg_me405 ME405 Code
This section contains 405 code

Included modules are
*  HW00 (\ref sec_hw0)
*  HW01 (\ref sec_hw1)
*  Lab 01 (\ref sec_lab1)
*  Lab 02 (\ref sec_lab2)
*  Lab 03 (\ref sec_lab3)
*  Lab 04 (\ref sec_lab4)

@subsection sec_hw0 Homework 00 
@image html me405hw00.jpg

@subsection sec_hw1 Homework 01: Python Review
This homework assignment was a review of python, specifically functions and tuples.
This .py computes the minimum number of bills/coins to return as change.
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/HW01/
*  Documentation: https://mkchandl.bitbucket.io/_h_w01_8py.html

@subsection sec_lab1 Lab 01
This lab was a review of finite state machines. See HW 0 for the State Transition Diagram used for this lab
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/Lab1/
*  Lab 1 Main Documentation: https://mkchandl.bitbucket.io/me405_lab1_8py.html
*  Lab 1 Shares Documentation: https://mkchandl.bitbucket.io/_l1shares_8py.html

@subsection sec_lab2 Lab 02
This lab involved using external interrupts to measure the reaction time of a user.
During ME 305 I ran into an issue handling overflow with my encoder driver. I would get
random negative values at semi-regular intervals. While I worked extensively with 
Professor Espinoza-Wade and Professor Revfem but we were unable to find the issue.
Because I'm using the same overflow as the encoder driver, I sometimes get random
negative reaction times. 
@image html ME405LAB2FSM.jpg
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/Lab2/ 
*  Lab 2 Main Documentation: https://mkchandl.bitbucket.io/me405_lab2_8py.html
*  Lab 2 Shares Documentation: https://mkchandl.bitbucket.io/me405_l2shares_8py.html
*  honestly don't really need a shares file but its here because I wrote the 
    main .py poorly the first time and it was easier to just keep the shares variables.

@subsection sec_lab3 Lab 03
This lab was focused on using a digital device to measure analog inputs and understanding 
serial communication. I had some trouble getting the nucleo adc object to correctly 
read voltage change by button press. It is unclear whether the issue is that there is 
some issue with the physical voltage on the board or if the issue is with the speed at
which the voltages are being read and saved to the buffer. I worked with Professor Wade 
and we were not able to resolve this issue. 
@image html ME405LAB3FSM.jpg
@image html graph2.jpg
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/Lab3/
*  Lab 3 Backend Documentation: https://mkchandl.bitbucket.io/me405lab3__back_8py.html
*  Lab 3 Frontend Documentation: https://mkchandl.bitbucket.io/me405lab3__front_8py.html
*  Lab 3 Shares Documentation: https://mkchandl.bitbucket.io/me405l3shares_8py.html

@subsection sec_lab4 Lab 04
This lab was to familairize ourselves with the i2c interface and i2c class in micropy
@image html graph.jpg
*  Source Code: https://bitbucket.org/hlbrooks/lab0x04
*  Lab 4 Main Documentation: https://mkchandl.bitbucket.io/me405lab4__main_8py.html
*  Lab 4 mcp9808 Documentation: https://mkchandl.bitbucket.io/mcp9808_8py.html

@page pg_me405_tp ME405 Term Project Code
This section contains 405 Term Project code and hand calculations

Included modules are
*  Lab 05 (\ref sec_lab5)
*  Lab 06 (\ref sec_lab6)
*  Lab 07 (\ref sec_lab7)
*  Lab 08 (\ref sec_lab8)
*  Lab 09 (\ref sec_lab9)

@subsection sec_lab5 Lab 05
This lab was a big review of intermediate dynamics. This lab involved mdoeling the
touchpad and base structure as a simple 4 bar linkage and perfomring a series of 
kinematics and kinetics to obtain a matrix equation describing the motion of the 
platform and ball.
@image html lab5_1.jpg
@image html lab5_2.jpg
@image html lab5_3a.jpg
@image html lab5_3b.jpg
@image html lab5_3c.jpg
@image html lab5_matrix.jpg

@subsection sec_lab6 Lab 06
This lab involved modeling the equations developed in lab 5 in matlab to create 
simulations. We were given different sets of initial conditions and plotted the 
responses of the simulation to assess their accuracy.
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/Lab6/

<CENTER><B> Open Loop Simulation
@image html lab6_1.jpg
The ball is initially at rest on a level platform directly above the center of gravity of the platform and there is no torque input from the motor. Run this simulation for 1 [s].
@image html lab6_2.jpg
The ball is initially at rest on a level platform offset horizontally from the center of gravity of the platform by 5 [cm] and there is no torque input from the motor. Run this simulation for 0.4 [s].
@image html lab6_3.jpg
The ball is initially at rest on a platform inclined at 5 degrees directly above the center of gravity of the platform and there is no torque input from the motor. Run this simulation for 0.4 [s].
@image html lab6_4.jpg
The ball is initially at rest on a level platform directly above the center of gravity of the platform and there is an impulse of 1 mNm*s applied by the motor. Run this simulation for 0.4 [s].

Closed Loop Simulation <P> 
</B> Each of these cases match the open loop simulation cases. </CENTER>
@image html lab6_CL1.jpg
@image html lab6_CL2.jpg
@image html lab6_CL3.jpg
@image html lab6_CL4.jpg

@subsection sec_lab7 Lab 07
This lab involved making a driver for the touchpad which could provide the coordinates of the point of contact on the pad.
It was important that this happen quickly and that center of the pad be the origin point. 
*  Source Code: https://bitbucket.org/mkchandl/me305_labs/src/master/ME405/Lab7/
*  Documentation: https://mkchandl.bitbucket.io/touchpad_driver_8py.html

@image html hwsetup1.jpg
@image html hwsetup2.jpg
hardware setup
    
@image html getx_py_results.jpg
<CENTER> the first collumn is the x position of my finger on the touchpad and the 2nd collumn is the elapsed time of the reading in us </CENTER>
@image html gety_py_results.jpg
<CENTER> the first collumn is the y position of my finger on the touchpad and the 2nd collumn is the elapsed time of the reading in us </CENTER>
@image html getz_py_results.jpg
<CENTER> the first collumn is the z position of my finger on the touchpad and the 2nd collumn is the elapsed time of the reading in us </CENTER>
@image html getxyz_py_results.jpg

@subsection sec_lab8 Lab 08
This lab was done with Ben Sahlin, his documentation is hosted at the following link: https://bsahlin.bitbucket.io/me405/page_Lab08.html 
This lab introduces encoder and motor control. The first goal of the lab was to create an encoder driver which can be used
 to measure the position and velocity of the output shafts and will be implemented in the balancing platform project. The 
 second goal of the lab was to create a motor driver which can be used to control the motors on the balancing platform, 
 including setting the duty cycle of the motors using PWM as well as managing fault detection in the H-Bridge chip. 
*  Source Code: https://bitbucket.org/bsahlin/me405_labs/src/master/Lab8/
*  Motor Driver Documentation: https://mkchandl.bitbucket.io/_t_p_motor_driver_8py.html
*  Encoder Driver Documentation: https://mkchandl.bitbucket.io/_t_p_encoder_driver_8py.html

@subsection sec_lab9 Lab 09
This lab was completed with Ben Sahlin, his documentation is hosted at the following link: https://bsahlin.bitbucket.io/me405/page_Lab08.htm
This lab was the second part of the term project which consisted of finding the 
gains for the closed loop controller, writing a finite state machine task to
balance the platform, and combining previously completed drivers and equations 
to comoplete the self balancing platform. 

<P> Video link: https://cpslo-my.sharepoint.com/:v:/g/personal/mkchandl_calpoly_edu/ESYQuaRNE1lOnVWfR1ZyHdsBmcyhT0ybj_oCtRV_E9-Iow

*  Source Code: https://bitbucket.org/bsahlin/me405_labs/src/master/Lab9/
*  Main file Documentation: https://mkchandl.bitbucket.io/main9_8py.html
*  Encoder Driver Documentation: https://mkchandl.bitbucket.io/_encoder_driver_8py.html
*  Motor Driver Documentation: https://mkchandl.bitbucket.io/_motor_driver_8py.html
*  FSM Task Documentation: https://mkchandl.bitbucket.io/_f_s_m___balance_platform_8py.html
*  State Space Controller Documentation: https://mkchandl.bitbucket.io/_state_space_controller_8py.html
*  Touch Panel Driver Documentation: https://mkchandl.bitbucket.io/_touch_panel_driver_8py.html
    
@author Michelle Chandler
'''