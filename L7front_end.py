# -*- coding: utf-8 -*-
"""
@file L7front_end.py

This is the front end spyder .py for lab 7

@author: Michelle Chandler
"""

import time
import serial 
import matplotlib.pyplot as plt



class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Each character received is checked and if it is an english
    letter a-z it is sent through a cipher in TaskCipher.
    
    @image html fsm_task_01.png width=1200px
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_SEND_GAIN        = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2
    
    ## Calculating error
    S3_CALC_ERROR       = 3
    
    ## Waiting state
    S4_PLOT             = 4
    
    ## Waiting state
    S5_NULL             = 5

    def __init__(self, taskNum, interval):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = interval
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval

        ## Serial port
        self.ser = serial.Serial(port= 'COM3', baudrate=115273, timeout=3)
        
        ## makind the velocity and position arrays that the measured values sent from the nucleo
        ## will be stored in        
        self.velocity = []
        self.position = []
        

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.time()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('state 0')
                self.transitionTo(self.S1_SEND_GAIN)
                self.time_ref = []
                self.velocity_ref = []
                self.position_ref = []
            
            elif(self.state == self.S1_SEND_GAIN):
                print('state 1')
                # Run State 1 Code
                self.gain = input('Provide a gain on the order of 1: ')
                self.gain = float(self.gain)
                # self.gain = int(self.gain*1e1)
                self.ser.write('{:}\r\n'.format(self.gain).encode())
                # to make sure no overlap between sending/recieving kp and sending/recieving omega_ref
                time.sleep(2)
                # open csv file
                self.ref = open('ref.csv');
                while True:
                    # Read a line of data. It should be in the format 't,v,x\n' but when the
                    # file runs out of lines the lines will return as empty strings, i.e. ''
                    line = self.ref.readline()
                    # If the line is empty, there are no more rows so exit the loop
                    if line == '':
                        break
                    # If the line is not empty, strip special characters, split on commas, and
                    # then append each value to its list.
                    else:
                        print('making vlociety, time, position ref arrays')
                        (t,v,x) = line.strip().split(',');
                        # add values from the csv file to respective time, position, velocity arrays
                        self.time_ref.append(float(t))
                        self.velocity_ref.append(float(v))
                        self.position_ref.append(float(x))
                # Can't forget to close the serial port
                self.ref.close()
                print('closed ref.csv')
                # self.ser.write(str(self.gain).encode())
                n = 0
                while n < len(self.velocity_ref):
                    print('n = ' +str(n))
                    print('sending velocity ref: ' + str(self.velocity_ref[n]))
                    #sending omega_ref values over to the nucleo one by one
                    self.ser.write('{:}\r\n'.format(self.velocity_ref[n]).encode())
                    # sleeping for .1 seconds between each send to make sure no values are missed by nucleo
                    time.sleep(0.1)
                    n += 1
                print('sent gain, ref velocity, entering sleep for 10 seconds')
                # sleeping for another 10 seconds to give nucleo plenty of time to run motors using omega_ref array
                time.sleep(10)
                self.transitionTo(self.S2_WAIT_FOR_RESP)
                
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                print('state 2')
                self.line_string = self.ser.readline().decode('ascii')
                print('recieved array line')
                print('in waiting  ' + str(self.ser.in_waiting))
                while self.ser.in_waiting !=0 :     
                    # Dealing with sent over velocity values
                    self.line_string = self.ser.readline().decode('ascii')
                    print('line string: ' + str(self.line_string))
                    self.line_list = self.line_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
                    print('line list: ' + str(self.line_list))
                    # Add the stipped and split values to velocity and position arrays. 
                    # I'm not sure why I have both float and int used here 
                    self.velocity.append(int(float(self.line_list[1])))
                    self.position.append(int(float(self.line_list[0])))
                    if self.ser.in_waiting == 0: 
                        self.transitionTo(self.S3_CALC_ERROR)
            
            elif(self.state == self.S3_CALC_ERROR):  
                # calculating error using given equation
                self.J = (1/len(self.velocity))*sum((self.velocity_ref-self.velocity)^2 + (self.position_ref-self.position)^2)
                print('J= ' + str(self.J))
                self.transitionTo(self.S4_PLOT)
                
            elif(self.state == self.S4_PLOT):
                self.transitionTo(self.S5_NULL)
                # Velocity first
                plt.subplot(2,1,1)
                plt.plot(self.time_ref,self.velocity)
                plt.ylabel('Velocity [RPM]')
                # Then position
                plt.subplot(2,1,2)
                plt.plot(self.time_ref,self.position)
                plt.xlabel('Time [s]')
                plt.ylabel('Position [deg]')
                print('Press stop to get plot')
                
            elif(self.state == self.S5_NULL):
                self.transitionTo(self.S5_NULL)
                # sits here after plots are made
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

UI = TaskUser(0, 1)

while True:
    UI.run()