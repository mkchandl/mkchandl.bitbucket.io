# -*- coding: utf-8 -*-
"""
@file me405lab3_back.py
@brief ME 405 lab 3 back end/ nucleo end
@date Jan 26 2021
@author: Michelle Chandler
"""

import pyb
from pyb import UART
import micropython
import array
import me405l3shares

# Create emergency buffer to store errors that are thrown
# inside callback
micropython.alloc_emergency_exception_buf(200)

## Create a timer object (timer 6) in general purpose counting mode
#  at 10 Hz
myTimer = pyb.Timer(2, freq=50000)

## Create an ADC object 
adc = pyb.ADC(pyb.Pin.board.PC0)

## Create a buffer to store the samples
buf  = array.array ('H', (0 for index in range (1000)))

## trigger variable
me405l3shares.trigger = 0

def myCallback(which_pin):
    '''
    @brief callback which runs when user presses button
    '''
    me405l3shares.trigger = 1

## Create external interrupt button
extint = pyb.ExtInt (pyb.Pin.board.PC13,      # Which pin
             pyb.ExtInt.IRQ_FALLING,      # Interrupt on FALLING edge
             pyb.Pin.PULL_NONE,             # Activate pullup resistor
             myCallback)                  # Interrupt service routine

## The serial communication set up
myuart = UART(2)

state = 0

while True:
    if state == 0: # wait for front end
        myuart.any() # look for characters to read from repl
        if myuart.any() != 0:
            cmd = myuart.readline() # save character sent from repl
            state = 1
    
    elif state == 1: # wait for button press
        if me405l3shares.trigger == 1: # once the external interrupt callback function is triggered
            adc.read_timed(buf, myTimer) # read values and save them to buffer
            if len(buf) >= 1000:
                state = 2
    
    elif state == 2: # send buf array to front end
        for n in range(len(buf)):
            myuart.write('{:}, {:}\r\n'.format(n, buf[n]))
        state = 3
        
    elif state == 3: # Break
        break
        

