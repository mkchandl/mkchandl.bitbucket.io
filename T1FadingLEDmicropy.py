# -*- coding: utf-8 -*-
"""
@file T1FadingLEDmicropy.py

Created on Fri Oct  9 11:21:15 2020

@author: CCHAN
"""

import utime
import pyb

class TaskLED:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of a physical LED, controlling its brightness.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_LED_INCREMENT      = 1    
    
    ## Constant defining State 2
    S2_LED_INCREMENT      = 2    
    
    ## Constant defining State 5
    S3_DO_NOTHING  = 3
    
    def __init__(self, interval, LED):
        '''
        @brief            Creates a TaskWindshield object.
        @param LED        An object from class PowerSupply representing the LED
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The power supply object that is on or off
        self.LED = LED
        
        ## The value of the fade
        #self.val = val
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        #self.buttonprepare()
        #self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        #self.tim2 = pyb.Timer(2, freq = 20000)
        #self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
       
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_LED_INCREMENT)
                self.LED.incr_val()
                print(str(self.runs) + ': State 0 ' + str(utime.ticks_diff(self.curr_time, self.start_time)))
            
            elif(self.state == self.S1_LED_INCREMENT):
                # Run State 1 Code
                self.transitionTo(self.S2_LED_INCREMENT)
                self.LED.incr_val()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_LED_INCREMENT):
                # Run State 2 Code
                self.transitionTo(self.S1_LED_INCREMENT)
                self.LED.incr_val()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S3_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    #def buttonprepare(self):
        
        #pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        

class TaskREPLLED:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of avirtual LED in the REPL.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_REPL_INIT                = 0    
    
    ## Constant defining State 1
    S1_REPL_INCREMENT      = 1    
    
    ## Constant defining State 2
    S2_REPL_INCREMENT      = 2    
    
    ## Constant defining State 5
    S3_DO_NOTHING          = 3
    
    def __init__(self, interval, REPL):
        '''
        @brief            Creates a Task REPL LED object.
        @param REPL        An object from class PowerSupply representing the REPL, or virtual LED
        '''
        
        ## The state to run on the next iteration of the task.
        self.REPL_state = self.S0_REPL_INIT
        
        ## The power supply object that is on or off
        self.REPL = REPL
        
        ## The value of the fade
        #self.val = val
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task on the REPL
        '''
       
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            
            if(self.REPL_state == self.S0_REPL_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_REPL_INCREMENT)
                self.REPL.rep_incr_val()
                print(str(self.runs) + ': State 0 ' + str(utime.ticks_diff(self.curr_time, self.start_time)))
            
            elif(self.REPL_state == self.S1_REPL_INCREMENT):
                # Run State 1 Code
                self.transitionTo(self.S2_REPL_INCREMENT)
                self.REPL.rep_incr_val()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.REPL_state == self.S2_REPL_INCREMENT):
                # Run State 2 Code
                self.transitionTo(self.S1_REPL_INCREMENT)
                self.REPL.rep_incr_val()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                
            elif(self.REPL_state == self.S3_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
class PowerSupply:
    '''
    @brief      A Power supply.
    @details    This class represents a power supply which provides different 
                levels of power to the LED.
    '''
    
    def __init__(self, val, max_val, tim2, t2ch1, pinA5):
        '''
        @brief          Creates a Power Supply Object
        @param val      A variable that represents the current percentage of max power supplied to LED
        @param max_val  A variable that represents the max percentage of power able to be suppleid to LED
        @param tim2     A variable that defines a pyb timer
        @param t2ch1    An object of the timer used for PWM, or pulse width modulation
        @param pinA5    A pin representing pinA%, or the user LED, on the nucleo
        '''
        ## This variable defines the current percentage of the power
        self.val = val
        ## This varaible describes the maximum percentage of the power
        self.max_val = max_val
        ## This varaible defines a timer
        self.tim2 = tim2
        ## This varaible describes a timer channel 
        self.t2ch1 = t2ch1
        ## This creates the pin object that represents the physical LED
        self.pinA5 = pinA5
    
    def incr_val(self):
        '''
        @brief Turns on LED at different levels of brightness from 10% to 100%
        '''
        self.t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        if self.val < self.max_val:
            self.val = self.val+10
            self.t2ch1.pulse_width_percent(self.val)
            # print(str(self.val))
        elif self.val >= self.max_val: 
            self.val = 0
            self.t2ch1.pulse_width_percent(self.val) 
            # print(str(self.val))

class REPLPowerSupply:
    '''
    @brief      A Power supply.
    @details    This class represents a power supply which provides different 
                levels of power to the virtual LED on the REPL.
    '''
    
    def __init__(self, rep_val, rep_max_val):
        '''
        @brief Creates a MotorDriver Object
        @param rep_val represents the virtual power supply percentage supplied to the LED
        @param rep_max_val represents the max virtual power supply able to be supplied to the lED
        '''
        pass
        self.rep_val = rep_val
        self.rep_max_val = rep_max_val
    
    def rep_incr_val(self):
        '''
        @brief Turns on LED
        '''
        if self.rep_val < self.rep_max_val:
            self.rep_val = self.rep_val + 1
            print(str(self.rep_val))
        elif self.rep_val >= self.rep_max_val: 
            self.rep_val = 0
            print(str(self.rep_val))

#defining the pin for the user LED
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) 
#defining the pyb timer for the nucleo
tim2  = pyb.Timer(2, freq = 20000)
# the object of the timer for PWM
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
# defining the power supply class object LED
LED = PowerSupply(0, 100, tim2, t2ch1, pinA5)
#REPL = REPLPowerSupply(0, 100)



Task1 = TaskLED(1, LED)
#Task2 = TaskREPLLED(1, REPL)


for N in range(100000000): # effectively while(True):
    Task1.run()
    #Task2.rum()






















