# -*- coding: utf-8 -*-
"""
@file Lab3Main.py  

This file is the main file that imports relevant .py files and creates relevant
objects for the classes

Created on Fri Oct 16 16:23:12 2020

@author: Michelle Chandler
"""
import pyb
import Lab3shares

from Lab3User import UserInterface_Task
from EncoderDriver import Encoder_Driver
#from Lab3 import EncoderTask
import Lab3

## Define the timer and timer channels
tim = pyb.Timer(3)
tim.init(prescaler=0, period = 65535)
tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)

## define the pinA6
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)

## define the pinA7
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)

## Creates a class encoder driver object
Driver = Encoder_Driver(tim, pinA6, pinA7, 65535)  

Task1 = Lab3.Encoder_Task(1, Driver)
Task2 = UserInterface_Task(1)


for N in range(100000000): 
    Task1.run()
    Task2.run()