# -*- coding: utf-8 -*-
"""
@file UIfront.py

Created on Sat Oct 31 09:43:21 2020

@author: CCHAN
"""

import serial 
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import L4shares
import time

ser = serial.Serial(port= 'COM3', baudrate=115273, timeout=1)
# L4shares.cmd = ser.write((3))
# L4shares.cmd = ser.write((3))


print('Enter G or g to begin collecting data, enter s to stop collection')

n=0
while(n==0):
    inv = input('Give me a character: ')
    if(inv=='G' or inv=='g'):
        L4shares.cmd = ser.write(str(inv).encode('ascii'))
        n = 1
    else:
        print('Please input G or g')
        
stop = 'a'
curr_timing = time.time()

if time.time() <= curr_timing + 10:
    if(stop != 's' and stop != 'S'):
        stop = input('Enter s or S to stop data collection: ')
        if(stop=='s' or stop=='S'):
            print('entered command s')
            L4shares.cmd = ser.write(str(stop).encode('ascii'))
            print('sent stop command')
            line_string = ser.readline().decode('ascii')
            print('recieved array line')
            print(str(ser.in_waiting))
            time.sleep(5)
            position = []
            time_= []
            while ser.in_waiting !=0 :     
                print('entering for loop  ' + str(ser.in_waiting))
                # count_p += 1    
                ## Dealing with stored position values
                print('recieving position array')
                line_string = ser.readline().decode('ascii')
                # position_string = ser.readline(L4shares.resp_p).decode('ascii') #reads single line, do I need to loop this? or will all of array be read
                print('strip array')
                line_list = line_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
                position.append(int(line_list[1]))
                time_.append(int(line_list[0]))
                if len(position) >= 50:
                    break
else: 
   L4shares.cmd = ser.write(str(stop).encode('ascii'))
   print('sent stop command')
   line_string = ser.readline().decode('ascii')
   print('recieved array line')
   print(str(ser.in_waiting))
   time.sleep(5)
   position = []
   time_= []
   while ser.in_waiting !=0 :     
        print('entering for loop  ' + str(ser.in_waiting))
        # count_p += 1    
        ## Dealing with stored position values
        print('recieving position array')
        line_string = ser.readline().decode('ascii')
        # position_string = ser.readline(L4shares.resp_p).decode('ascii') #reads single line, do I need to loop this? or will all of array be read
        print('strip array')
        line_list = line_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
        position.append(int(line_list[1]))
        time_.append(int(line_list[0]))
        if len(position) >= 50:
            break 
       

# count_p = 0
# for line in L4shares.resp_p:
#     count_p += 1    
#     ## Dealing with stored position values
#     position_string = ser.readline().decode('ascii')
#     # position_string = ser.readline(L4shares.resp_p).decode('ascii') #reads single line, do I need to loop this? or will all of array be read
#     position_list = position_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
#     position = []
#     position.append(int(position_list[0]))
                    
# count_t = 0
# for line in L4shares.resp_t:
#     count_t += 1
#     ##Dealing with stored time values
#     #look at readlines 
#     #readline inside the loop
#     #could read fixed number of lines bc know number of data points, ie 51
#     time_string = ser.readline(L4shares.resp_t).decode('ascii') #reads single line, do I need to loop this? or will all of array be read
#     time_list = time_string.strip().split(',') # strip any special characters and then split the string into wherever a comma appears; 
#     time_= ()
#     time_.append(int(time_list[0]))
                    
## reset shared variables
L4shares.resp_p = None    
L4shares.resp_t = None  
                    
## Plotting Data from Encoder        
fig, ax = plt.subplots()  # Create a figure containing a single axes.
ax.plot(time_, position)  # Plot some data on the axes. 
ax.set_xlabel('Time, t [s]')
ax.set_ylabel('Position [degrees]')
ax.set_title("Lab 4 Encoder Position over time")
                    
## Saving the two nicropy arrays as a 2d numpy array
arr = np.array([time_, position])
## Using the numpy array to save the data as a CSV file
np.savetxt('encoderposition.csv', arr, delimiter = ',', fmt = '%d', header = 'Encoder Position versus time')