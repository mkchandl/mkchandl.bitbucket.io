# -*- coding: utf-8 -*-
'''
@file       TPEncoderDriver.py
@brief      EncoderDriver object class
@details    Includes EncoderDriver class that defines functions for the EncoderDriver
            object, allowing it to be controlled by the finite state machine.

@author     Michelle Chandler
@author     Ben Sahlin
@date       March 2, 2021
'''

import pyb
from pyb import Pin

class EncoderDriver:
    
    def __init__ (self, ch1_pin, ch2_pin, timer, ticks_per_deg):
        '''
        @brief                  Creates an EncoderDriver object
        @details                Creates EncoderDriver object with specified 
                                parameters, sets initial position and initial 
                                shared variable values.
        @param timer_number     Integer representing timer number to use
        @param ch1_pin_name     String representing Channel 1 Pin to use
        @param ch2_pin_name     String representing Channel 2 Pin to use
        @param ch1_number       Integer representing Channel 1 Number to use
        @param ch2_number       Integer representing Channel 2 Number to use
        @param ticks_per_deg    Encoder ticks per degree defined by hardware
        '''
        self.debug = False #print extra debugging statements
        #set up inputted parameters
        self.ch1_pin = ch1_pin
        self.ch2_pin = ch2_pin
        self.tim = timer
        self.ticks_per_deg = ticks_per_deg

        #create timer object and channels corresponding to pin objects
        #self.tim = pyb.Timer(self.timer_number)
        self.tim.init(prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin = self.ch1_pin, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin = self.ch2_pin, mode = pyb.Timer.ENC_AB)
        
        #set up initial position at 0, also sets initial delta
        self.set_position(0)
        self.measured_position = [0,0]
                
    def update(self):
        '''
        @brief      Updates encoder position
        @details    Finds new encoder position based on current measured 
                    position, taking into account timer overflow. Also checks 
                    to see if encoder should be reset, and updates shares 
                    variables.
        @returns    Returns an integer of the current position of encoder in ticks
        '''
        #move current measured position to old measured position
        self.measured_position[0] = self.measured_position[1]
        #update current measured position
        self.measured_position[1] = self.tim.counter()
        #calc initial measured delta, could be bad delta
        self.delta = self.measured_position[1] - self.measured_position[0]
        if (abs(self.delta) > 0.5*self.tim.period()):
            #bad delta value
            if (self.delta>0):
                #going CW, will have positive large delta that should be negative
                self.delta = self.delta - self.tim.period()
                self.debugprint('cw adjusted delta = '+str(self.delta))
            elif (self.delta<0):
                #going CCW, will have negative large delta that should be positive
                self.delta = self.delta + self.tim.period()
                self.debugprint('ccw adjusted delta = '+str(self.delta))
            else:
                print('Delta calc error')
                
        #move current position to old position
        self.position[0] = self.position[1]
        #update to real current position based on delta
        self.position[1] = self.position[0] + self.delta
        return(self.position[1]) #returns only current position
    
    def get_delta(self):
        '''
        @brief      Returns and sets current encoder delta in ticks
        @details    Calculates delta based on previous and current encoder 
                    positions.
        @returns    Returns an integer of the current delta of the encoder in ticks
        '''
        #delta = new position - old position
        self.delta = self.position[1]-self.position[0]
        #self.debugprint('delta subraction results in '+str(self.delta))             
        return self.delta
    
    def get_position(self):
        '''
        @brief      Returns current encoder position in ticks
        @details    Returns current position without running update function
        @returns    Returns an integer of the current position of encoder in ticks
        '''
        return self.position[1]
    
    def set_position(self, desired_position):
        '''
        @brief                      Sets encoder position to desired position
        @details                    Sets encoder previous and current positions to inputted 
                                    position and recalculates delta.
        @param desired_position     Variable representing a position to set encoder to.
        '''
        self.tim.counter(desired_position)
        self.position = [desired_position, desired_position]
        self.delta = self.get_delta()
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)
            
if __name__ == '__main__':
    #test code for encoder
    #create pin objects for encoder 1
    e1_ch1_pin = Pin(Pin.cpu.B6)
    e1_ch2_pin = Pin(Pin.cpu.B7)
    #create pin objects for encoder 2
    e2_ch1_pin = Pin(Pin.cpu.C6)
    e2_ch2_pin = Pin(Pin.cpu.C7)
    #create timer object for encoder 1
    tim4 = pyb.Timer(4)
    #create timer object for encoder 2
    tim8 = pyb.Timer(8)
    #output shaft ticks/deg = 1000 cyc/rev * 4 ticks/cyc * 1 rev/360 deg)
    ticks_per_deg = 1000*4/360
    #create encoder objects for encoders 1 and 2
    enc1 = EncoderDriver(e1_ch1_pin, e1_ch2_pin, tim4, ticks_per_deg)
    enc2 = EncoderDriver(e2_ch1_pin, e2_ch2_pin, tim8, ticks_per_deg)
    while True:
        print('{:} and {:}, delta1 = {:}'.format(enc1.update(), enc2.update(), enc1.get_delta()))