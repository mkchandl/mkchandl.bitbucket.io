# -*- coding: utf-8 -*-
"""
@file
@author: Michelle Chandler
@brief ME 405 Lab 1 .py
@date Created on Tue Jan 12 12:13:58 2021

"""

import keyboard
import L1shares
L1shares.state = 0
my_price = None
my_payment = 8*[0]
drink = 0

def on_keypress (thing):
    '''
    @brief Callback
    '''
    L1shares.userinput = thing.name

def getChange(price, payment):
    '''
    @brief A function that calculates the change required in the least number of coins
    @param price an integer representing the price of an object in cents
    @param payment a tuple representing the amount paid and what types of coins/dollars were used
    @return if funds are sufficient returns tuple of change if funds are insufficient
            returns none
    '''
    final_change = 8*[0]
    check_payment = 1*payment[0]+5*payment[1]+10*payment[2]+25*payment[3]+100*payment[4]+100*5*payment[5] + 1000*payment[6] + 100*20*payment[7]
    print('check_payment: ' + '$'+str(check_payment/100))
    check_price = price #cents
    print('check_price: ' + '$' + str(check_price/100))
    
    if price <= 0:
        print('please input a positive number')
        
    if check_payment < check_price:
        print('insufficient funds, please insert more $$')
        L1shares.state = 1
        
    elif check_payment == check_price:
        print('exact payment, no change required')
        L1shares.state = 0
        
    else:
        extra_change = check_payment - check_price #cents
        # check units: what is in cents and what is in dollars. keep all in cents
        change = extra_change #cents
        
        twenties = change/(20*100)
        if twenties > 1:
            twenties = int(twenties)
            final_change[7] = twenties
            change = change - twenties*20*100
        else:
            pass
            
        tens = change/(10*100)
        if tens > 1:
            tens = int(tens)
            final_change[6] = tens
            change = change - tens*10*100
        else:
            pass
        
        fives = change/(5*100)
        if fives > 1: 
            fives = int(fives)
            final_change[5] = fives
            change = change- fives*5*100
        else:
            pass
        
        ones = change /(1*100)
        if ones > 1:
            ones = int(ones)
            final_change[4] = ones
            change = change - ones*100
        else:
            pass
        
        quarters = change/(25)
        if quarters > 1:
            quarters = int(quarters)
            final_change[3] = quarters
            change = change - quarters*25
        else:
            pass
        
        dimes = change/10
        if dimes > 1:
            dimes = int(dimes)
            final_change[2] = dimes
            change = change - dimes*10
        else:
            pass
            
        nickles = change/5
        if nickles > 1:
            nickles = int(nickles)
            final_change[1] = nickles
            change = change - nickles*5
        else:
            pass

        pennies = change/1
        if pennies > 1:
            final_change[0] = pennies
        else:
            pass
            
        print('returned change: ' + str(final_change))
        L1shares.state = 0

def printWelcome():
    '''
    @brief prints welcome message
    '''
    print('Invalid input, please select from the following:')
    print('press c for Cuke: $1.00')
    print('press p for Popsi: $1.20')
    print('press s for Spryte: $0.85')
    print('press d for Dr. Pupper: $1.10')
    print('press 0 to enter $0.01')
    print('press 1 to enter $0.05')
    print('press 2 to enter $0.10')
    print('press 3 to enter $0.25')
    print('press 4 to enter $1.00')
    print('press 5 to enter $5.00')
    print('press 6 to enter $10.00')
    print('press 7 to enter $20.00')
    
    
while True:
    if L1shares.state == 0: 
        # Initializes parameters
        # Sets price to None
        my_price = None
        # Creates an array of zeros for the payment
        my_payment = 8*[0]
        # Drink selction variable is set to no drink selected
        drink = 0
        printWelcome()
        # Begins keyboard module. Monitors key presses
        keyboard.on_press (on_keypress)
        L1shares.state = 1
        
    elif L1shares.state == 1:
        try:
        # wait for user input state
            if L1shares.userinput == "s":
                print ('Spryte selected, price: $0.85')
                my_price = 85
                drink = 1
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "c":
                print ('Cuke selected, price: $1.00')
                my_price = 100
                drink = 2
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "p":
                print ('Popsi selected, price: $1.20')
                my_price = 120
                drink = 3
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "d":
                print ('Dr Pupper selected, price: $1.10')
                my_price = 110
                drink = 4
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "0":
                my_payment[0] = my_payment[0]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "1":
                my_payment[1] = my_payment[1]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "2":
                my_payment[2] = my_payment[2]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "3":
                my_payment[3] = my_payment[3]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "4":
                my_payment[4] = my_payment[4]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "5":
                my_payment[5] = my_payment[5]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "6":
                my_payment[6] = my_payment[6]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "7":
                my_payment[7] = my_payment[7]+1
                print('balance: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 2
                
            elif L1shares.userinput == "e":
                print('returning change: ' + str(my_payment))
                L1shares.userinput = None
                L1shares.state = 0
            else:
                pass
        except KeyboardInterrupt:
            break
                
    elif L1shares.state == 2:
        # check if sufficient funds
        if my_price == None:
            print('please select drink:')
            print('enter c for cuke')
            print('enter p for pupsi')
            print('enter s for spryte')
            print('enter d for dr pupper')
            #user input code here ?
            L1shares.state = 1
        elif my_payment == 8*[0] and drink != 0:
            if drink == 1:
                print('spryte selected, please insert money')
                L1shares.state = 1
            if drink == 2:
                print('cuke selected, please insert money')
                L1shares.state = 1  
            if drink == 3:
                print('popsi selected, please insert money')
                L1shares.state = 1
            if drink == 4:
                print('dr pupper selected, please insert money')
                L1shares.state = 1
        else:
            
            getChange(my_price, my_payment)
 
        