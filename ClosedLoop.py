# -*- coding: utf-8 -*-
"""
@file ClosedLoop.py

This is a closed loop motor speed controller used in lab 6 and 7
Created on Tue Nov 17 13:23:27 2020

@author: Michelle Chandler
"""

import L6shares
from pyb import UART

class Closed_Loop:
    ''' 
    @brief This class implements a closed loop for speed control
    '''

    def __init__(self):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        '''
        
        
        
        self.myuart = UART(2)
        

    def update(self, delta_rpm, omega_ref, gain):
        '''
        @brief This method sets the motor sleep pin high
        '''
        
        # print('Loop Update()')
        # print('L6shares.delta_rpm: ' + str(delta_rpm) )
        self.omega_err = omega_ref - delta_rpm
        # print('omega_err= ' + str(self.omega_err))
        
        return int((gain/3.3)*self.omega_err)
        
        
    def get_Kp(self):
        '''
        @brief This method sets the motor sleep pin low
        '''        
        print(str(L6shares.gain))
        
    def set_Kp(self, gain):
        self.gain = gain
    

    

