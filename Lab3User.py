# -*- coding: utf-8 -*-
"""
@file Lab3User.py

Created on Wed Oct 14 16:25:46 2020

@author: CCHAN
"""

import utime
from pyb import UART
import pyb
import Lab3shares

class UserInterface_Task:
    '''
    @brief    Task for the encoder     
    @details  Task that takes in user input
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2 
    
    def __init__(self, interval): 
        '''
        @brief    the task that checks for user input and sends results of commands  
        '''  
        
        ## The serial communication set up
        self.myuart = UART(2)
        #self.myuart = pyb.uart(1, 9600)                         # init with given baudrate
        self.myuart.init(9600, bits=8, parity=None, stop=1) # init with given parameters

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        print('Available Commands: ')
        print('Zero:            z')
        print('Get Position:    p')
        print('Get Delta:       d')
         
    def run(self): 
        '''
        @brief runs one iteration of the task where class encoder driver update method is called
        '''
        
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code
                if self.myuart.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    Lab3shares.cmd = self.myuart.readchar()
                    
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                if Lab3shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    self.myuart.writechar(Lab3shares.resp)
                    Lab3shares.resp = None       
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState   
        
