# -*- coding: utf-8 -*-
"""
@file me405L2shares.py
@brief the shares file for me 405 lab 2
@author: Michelle Chandler
@date Created on Sun Jan 24 12:11:52 2021

"""

delta = None
stop_count = None
start = None
period = 0x7FFFFFFF
rxn_time = None