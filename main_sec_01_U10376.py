'''
@file main_sec_01.py

@image html elevatorFSM.jpeg
'''

from HW0_elevator import Button, MotorDriver, TaskElevator


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
Floor1Sensor1 = Button('PB6')
Floor2Sensor1 = Button('PB7')
Floor1Button1 = Button('PB8')
Floor2Button1 = Button('PB9')
Motor1 = MotorDriver()

Floor1Sensor2 = Button('PB6')
Floor2Sensor2 = Button('PB7')
Floor1Button2 = Button('PB8')
Floor2Button2 = Button('PB9')
Motor2 = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, Floor1Sensor1, Floor2Sensor1, Floor1Button1, Floor2Button1, Motor1)
task2 = TaskElevator(0.1, Floor1Sensor2, Floor2Sensor2, Floor1Button2, Floor2Button2, Motor2)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()