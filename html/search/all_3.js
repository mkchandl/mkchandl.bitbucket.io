var searchData=
[
  ['calc_5fvelocity_12',['calc_velocity',['../class_encoder_driver_1_1_encoder_driver.html#a620ea7d063c9847cc325a1f10efce026',1,'EncoderDriver::EncoderDriver']]],
  ['calctorque_13',['calcTorque',['../class_state_space_controller_1_1_state_space_controller.html#a5685ecd844434b357e915a2c286865ef',1,'StateSpaceController::StateSpaceController']]],
  ['celsius_14',['celsius',['../classmcp9808_1_1mcp9808.html#a2aa3aa0a45168d84968c7a36d7cdd542',1,'mcp9808::mcp9808']]],
  ['check_15',['check',['../classmcp9808_1_1mcp9808.html#aa432b699bc45c275cf257ec55207285a',1,'mcp9808::mcp9808']]],
  ['checkany_16',['CheckAny',['../class_b_l_e___driver_1_1_b_l_e___driver.html#a44f444ec88e76ca02f83b794bef966e6',1,'BLE_Driver.BLE_Driver.CheckAny()'],['../class_l5main_1_1_b_l_e___driver.html#af16adba2ca1b5e477267e94f94670ade',1,'L5main.BLE_Driver.CheckAny()']]],
  ['checkinput_17',['CheckInput',['../class_b_l_e___driver_1_1_b_l_e___driver.html#ae60efbacfeba4de96c61607d45b86cb3',1,'BLE_Driver.BLE_Driver.CheckInput()'],['../class_l5main_1_1_b_l_e___driver.html#a46ae86105f852b723acc3dcc0f447f79',1,'L5main.BLE_Driver.CheckInput()']]],
  ['closed_5floop_18',['Closed_Loop',['../class_closed_loop_1_1_closed___loop.html',1,'ClosedLoop']]],
  ['closedloop_2epy_19',['ClosedLoop.py',['../_closed_loop_8py.html',1,'']]],
  ['count_20',['count',['../class_b_l_e___task_1_1_b_l_e___task.html#a24d7910abae3d01e71a4041b35d3a08f',1,'BLE_Task.BLE_Task.count()'],['../class_l5main_1_1_b_l_e___task.html#a58a24ee861b8017336b040527ea24edb',1,'L5main.BLE_Task.count()']]],
  ['curr_5fcount_21',['curr_count',['../class_l6_encoder_driver_1_1_encoder___driver.html#aa7f1bb31047c20fe092c6ab161cc51f6',1,'L6EncoderDriver.Encoder_Driver.curr_count()'],['../class_l7_encoder_driver_1_1_encoder___driver.html#a67280dcdb1780167acfb0dc47136dc70',1,'L7EncoderDriver.Encoder_Driver.curr_count()'],['../classmain_1_1_encoder___driver.html#a23d12c9c3bc69a5c7558147c5f856784',1,'main.Encoder_Driver.curr_count()']]],
  ['curr_5ftime_22',['curr_time',['../class_l7front__end_1_1_task_user.html#a95e57dd265444366fd9ce7e034f248e9',1,'L7front_end.TaskUser.curr_time()'],['../class_u_ifront__end_1_1_task_user.html#a4aa4e6ee70d7bb977c71032d601a730d',1,'UIfront_end.TaskUser.curr_time()']]]
];
