# -*- coding: utf-8 -*-
"""
@file L7EncoderDriver.py

This file is the encoder driver for lab 7
Created on Fri Oct 16 16:21:37 2020

@author: Michelle Chandler
"""
import L7shares

class Encoder_Driver:
    '''
    @brief A Class that drives the encoder
    @details The Ecoder Driver class is in charge of keeping track of the 
             position of the motor and sometimes resetting it
    '''
    
    def __init__(self, tim, pinB6, pinB7, period):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA6    pin on nucleo
        @param pinA7    pin on nucleo
        '''
        self.period = period
        #self.period = 0xFFFF
        
        ## 
        self.tim = tim
        
        ## Pin object used for Channel 1
        self.pinB6 = pinB6
         
        
        ## Pin object used for Channel 2 
        self.pinB7 = pinB7
         
        
        ## Initialize variable curr_count
        self.curr_count = self.tim.counter()
            
        ## Initialize the position variable
        L7shares.position = self.curr_count
        L7shares.resp_p.append(L7shares.position)
        self.delta = 0
        
    def update(self, delta_rpm, position, interval):  
        '''
        @brief Updates position variable, handles bad deltas
        '''
        # print('calling enocder driver update')
        
        ## This method is called regularly to update the recorded position of the encoder
        ## assign the value returned by tim.counter to variable count
        self.last_count = self.curr_count
        self.curr_count = self.tim.counter()
        
        ## variable finding the delta between the two most recent counts
        self.delta = self.curr_count - self.last_count
        
        ## Deals with bad deltas
        if self.delta > self.period/2:
            # delta -= self.period-1
            self.delta -= self.period
                
        elif self.delta < -self.period/2:
            # delta += self.period+1
            self.delta += self.period

        ## saves the updated position
        L7shares.position += self.delta
        
        ## Unit conversion for delta
        # L6shares.delta_rpm = int((delta*60000)/(4000*10*interval*1e-3))
        L7shares.delta_rpm = (self.delta*60000)/(4000*interval)
        
    def get_position(self): 
        '''
        @brief This method returns the most recently updated position of the encoder
        '''
        print(str(L7shares.position))
        
    def set_position(self):
        '''
        @brief Resets the position variable to zero
        '''
        self.position = 0
        print(str(self.position))
        
    def get_delta(self):
        '''
        @brief Calculates delta and corrects bad deltas
        '''
        print(self.delta)
        

if __name__ == '__main__':
    
    import pyb
    import MotorDriver
    tim = pyb.Timer(3, freq=20000);
    tim4 = pyb.Timer(4, prescaler=0, period = 65535);
    ## define the pinB6
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    IN1_pin = pyb.Pin(pyb.Pin.cpu.B4)
    IN2_pin = pyb.Pin(pyb.Pin.cpu.B5)
    EncDriver = Encoder_Driver(tim4, pinB6, pinB7, 65535)
    MotDriver = MotorDriver.Motor_Driver(sleep, IN1_pin, IN2_pin, tim) 
    interval = .02
    
    MotDriver.enable()
    while True:
        for n in range(20):
            MotDriver.set_duty(40)
            EncDriver.update(L7shares.delta_rpm, L7shares.position, interval)
            EncDriver.get_position()
    MotDriver.disable()
    