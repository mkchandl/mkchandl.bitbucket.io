'''
@file ME350Lab01.py 

Fibbonacci Sequence Number Generator

Detailed doc for ME350Lab1 

@author Michelle Chandler

@date September 26, 2020

'''

from numpy import zeros

input = print ('please enter positive integer in the form fib(X): ')

def fib (idx):
    '''
    @brief      this function returns a value of the fibonacci sequence specified by user
    @details    This function takes an integer input from a user and uses the input
                to get the value of the fibonacci sequence that corresponds to the
                inputted index
    @param idx  idx is the index used in the calculations suplied by the user
    '''
    print ('Calculating Fibonacci number at index n = {:}.'.format(idx))
    thing = zeros([idx + 1])
    ind = idx
    try:
        ind = int(ind)
    except ValueError:
            print("No.. input is not a number. It's a string") 
    if idx < 0:
        print('Please enter a positive integer')
    else: 
        if ind == 0:
            thing[0]=0 
        elif ind == 1:
            thing[0]=0
            thing[1]=1
        else:
            thing[0]=0
            thing[1]=1 
            n = 2
            while n <= idx: 
                thing[n]=thing[n-1]+thing[n-2]
                n = n+1
    print(thing[idx])
    
n = 1
if n == 1:
    fib(4)