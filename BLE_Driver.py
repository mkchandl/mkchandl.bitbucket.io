# -*- coding: utf-8 -*-
"""
@file BLE_Driver.py

Created on Fri Nov  6 11:29:52 2020

@author: Michelle Chandler
"""
from pyb import UART
import L5shares

class BLE_Driver:
    '''
    @brief      A Class that drives bluetooth communication and LED state
    @details    The BLE Driver class takes input over bluetooth from a phone app, 
                saves the data to be used by the BLE task, and sends a message 
                back to the phone app
    '''
    
    def __init__(self, pinA5):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA5    pin on nucleo
        '''
        
        ## Pin object used for Channel 1
        self.pinA5 = pinA5

        ## Uart communication 
        self.myuart = UART(3, baudrate = 9600)
        
    def CheckInput(self):  
        '''
        @brief checks for character from phone, saves character, send message to phone
        '''
        
        if self.myuart.any():
            L5shares.cmd = None
            L5shares.cmd = self.myuart.readchar()
            # Writing back to phone
            L5shares.resp.append(L5shares.cmd)
            self.myuart.write(L5shares.resp)
        else:
            L5shares.cmd = None
            
    def CheckAny(self):
        ''' 
        @brief THis method checks if there are any characters to be read
        '''
        L5shares.check_any = None
        # variable containing the number of characters left to be read 
        L5shares.check_any = self.myuart.any()
        
    def ReadAny(self):
        ''' 
        @brief THis method reads any characters that need to be read
        ''' 
        L5shares.cmd = None
        L5shares.cmd = self.myuart.readchar()
        # Writing back to phone
        L5shares.resp = ('LED blinking in Hz at')
        L5shares.resp.append(L5shares.cmd)
        self.myuart.write(L5shares.resp)
        
    def LEDsame_state(self): 
        '''
        @brief This method does not change the state of the LED
        '''
        # If the LED is on it stays on
        if self.pinA5.high():
            self.pinA5.high()
        # If the lED is off it stays off    
        elif self.pinA5.low():
            self.pinA5.low()
        
    def LEDchange_state(self):
        '''
        @brief This method changes the state of the LED
        '''
        # If the LED is on it turns off
        if self.pinA5.high():
            self.pinA5.low()
        # If the lED is off it turns on   
        elif self.pinA5.low():
            self.pinA5.high()
