# -*- coding: utf-8 -*-
"""
@file BLE_Task.py

Created on Fri Nov  6 12:07:51 2020

@author: Michelle Chandler
"""

import utime
import pyb
import L5shares

class BLE_Task:
    '''
    @brief      A finite state machine to control the blinking frequnecy of an LED.
    @details    This class implements a finite state machine to control the
                operation of an LED, taking user input and using that input to 
                set the frequency of the LED blinking.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_WAIT_FOR_CMD     = 1    
    
    ## Constant defining State 2
    S2_BLINK_AT_FREQUENCY      = 2    
    
    ## Constant defining State 5
    S3_DO_NOTHING  = 3
    
    def __init__(self, interval, BLE_Driver_object):
        '''
        @brief                      Creates a BLE task object.
        @param BLE_Driver_object    An object from class BLE Driver
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The power supply object that is on or off
        self.BLE_Driver_object = BLE_Driver_object
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Counter for state 2. Is re-zeroed at the end of state 1
        self.count = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        

        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        #self.buttonprepare()
        #self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        #self.tim2 = pyb.Timer(2, freq = 20000)
        #self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
       
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code 
                self.BLE_Driver_object.CheckInput()
                if L5shares.cmd != None:
                    self.transitionTo(self.S2_BLINK_AT_FREQUENCY)
                    # Initialize the frequency value
                    self.frequ = L5shares.cmd
                    self.count = 0
                else:
                   self.transitionTo(self.S1_WAIT_FOR_CMD) 
            
            elif(self.state == self.S2_BLINK_AT_FREQUENCY):
                # Run State 2 Code
                # check for any input from phone using the check any method from
                # the ble driver class
                self.BLE_Driver_object.CheckAny()
                # if user input was found
                if L5shares.check_any != None:
                    # reads and saves the user input using the read any method from
                    # the ble driver class
                    self.BLE_Driver_object.ReadAny()
                    # saves the user input as a new variable for frequency
                    self.frequ = L5shares.cmd
                else:
                    # val is a variable that represents the seconds the light 
                    # remains at one state, in seconds.
                    self.val = 1/self.frequ
                    # these conditionals determine whether or not the LED remains
                    # in its current state or changes state
                    if (self.count < self.val):
                        self.BLE_Driver_object.LEDsame_state()
                        print('LED same state') 
                    elif (self.count > self.val):
                        self.BLE_Driver_object.LEDchange_state()
                        self.count = 0
                        print('LED change state')
                
            elif(self.state == self.S3_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
            # Specifying the next cound
            self.count = utime.ticks.add(self.interval, self.count)
            print(str(self.count))
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState