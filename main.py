# -*- coding: utf-8 -*-
"""
@file main.py

This file is the main file for lab 4. Because of issues importing things with 
lab 3, all relevant classes are contained in this file. This .py contains: 
encoder driver class, data generation class, the main .py where I define 
objects and tasks and also import the User Interface task UI which deals with 
waiting and watching for commands

Created on Sun Oct 25 15:12:34 2020

@author: Michelle Chandler
"""

# -*- coding: utf-8 -*-
"""

"""
import L4shares
#import array
import utime
from pyb import UART
import pyb 
## importing a user interface to run on nucleo so can hopefully run whole lab on 
##nucleo before trying to run UI on spyder
# import UI

class Encoder_Driver:
    '''
    @brief A Class that drives the encoder
    @details The Ecoder Driver class is in charge of keeping track of the 
             position of the motor and sometimes resetting it
    '''
    
    def __init__(self, tim, pinA6, pinA7, period):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA6    pin on nucleo pin on nucleo connected to encoder
        @param pinA7    pin on nucleo connected to encoder
        '''
        self.period = period
        #self.period = 0xFFFF
        
        ## 
        self.tim = tim
        
        ## Pin object used for Channel 1
        self.pinA6 = pinA6
         
        
        ## Pin object used for Channel 2 
        self.pinA7 = pinA7
         
        
        ## Initialize variable curr_count
        self.curr_count = self.tim.counter()
            
        ## Initialize the position variable
        L4shares.position = self.curr_count
        L4shares.resp_p.append(L4shares.position)
        self.delta = 0
        
    def update(self):  
        '''
        @brief Updates position variable, handles bad deltas
        @details    This method is called regularly to update the recorded 
                    position of the encoder
        '''
        ## This method is called regularly to update the recorded position of the encoder
        ## assign the value returned by tim.counter to variable count
        self.last_count = self.curr_count
        self.curr_count = self.tim.counter()
        
        ## variable finding the delta between the two most recent counts
        self.delta = self.curr_count - self.last_count
        
        ## Deals with bad deltas
        if self.delta > self.period/2:
            self.delta -= self.period
                
        elif self.delta < -self.period/2:
            self.delta += self.period
            
        L4shares.position += self.delta
        

## becuase this is a combination of .py files, this is from the main.py
## Define the timer and timer channels
tim = pyb.Timer(3)
tim.init(prescaler=0, period = 65535)
tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)

## define the pinA6
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)

## define the pinA7
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)
      
Driver = Encoder_Driver(tim, pinA6, pinA7, 65535)         

## This is 
  
class DataGen_Task:
    '''
    @brief    Task for the encoder     
    @details  Task that calls update funct using class encoder driver object
                and then stores the most recent saved position and time values in 2 arrays
                filling up the arrays column by collumn each interation
                The storage is triggered by a command from the user, either G/S
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Waitign for character state
    S1_WAIT_FOR_CMD    = 1
    
    ## Waiting for response state
    S2_CALL_UPDATE    = 2 
    
    S3_PRINT_RESP     = 3
    
    def __init__(self, interval, Driver): 
        '''
        @brief    for the data generation/ storage class
        @param Driver is a Class Encoder Driver object
        '''  
        
        ## The serial communication set up
        self.myuart = UART(2)
        #self.myuart = pyb.uart(1, 9600)                         # init with given baudrate
        #self.myuart.init(9600, bits=8, parity=None, stop=1) # init with given parameters

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The number of values placed into the position and time array
        ## used to determine when to start sending array data to computer 
        self.val_num = 0 
        
        # The 'timestamp' for when the task was run
        L4shares.time = utime.ticks_add(self.val_num, self.interval)
        

        
    def run(self): 
        '''
        @brief runs one iteration of the task where class encoder driver update method is called
        '''
        
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                # print('State 0')
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # print('State1')
                # Run State 1 Code
                # print('about to check for character')
                if self.myuart.any() != 0:
                    # print('checked for character')
                    L4shares.cmd = self.myuart.readchar()
                    # print('assigned character' + str(L4shares.cmd) + 'to cmd')
                    # Deals with a given G command
                    if L4shares.cmd == 103: 
                        # print('transitioning to S2')
                        self.transitionTo(self.S2_CALL_UPDATE)
                        self.val_num = 0
                    # Deals with a given S command   
                    elif (L4shares.cmd == 115 or self.curr_time - self.start_time >=10000):
                        # print('transitioning to S3')
                        self.transitionTo(self.S3_PRINT_RESP)
                    # Deals with commands other than S and G
                    else:
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                
                else:
                    # print('checked, no character, returning to top of state1')
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
            elif(self.state == self.S2_CALL_UPDATE):
                # Run State 2 Code
                # print('This is state2, calling update')
                Driver.update()  
                # print('Called update')
                L4shares.resp_p.append(L4shares.position)
                L4shares.resp_t.append(L4shares.time)
                # print(' added to array')
                # self.val_num += 1
                # print('val_num: ' + str(self.val_num))
                if self.val_num == 50:
                    # print('done filling array, going to S3')
                    self.transitionTo(self.S3_PRINT_RESP)
                    
            elif(self.state == self.S3_PRINT_RESP):
               # Run State 3 Code
               # sends saved data back to front end
                n = 0
                # print('length of rep_p: ' + str(len(L4shares.resp_p)))
                # print('length of rep_t: ' + str(len(L4shares.resp_t)))
                for n in range(len(L4shares.resp_t)):
                    self.myuart.write('{:}, {:}\r\n'.format(L4shares.resp_t[n], L4shares.resp_p[n]))
                    # print(L4shares.resp_t[n], L4shares.resp_p[n])
                    
                
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # specifying the next value of the variable time that will be saved in the time array
            L4shares.time = utime.ticks_add(self.val_num, self.interval)
            self.val_num += 1
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState   


# task0 = UI.TaskUser(0, 1_000, dbg=False)
task1 = DataGen_Task(.2, Driver) 

# taskList = [task0,
#             task1] 

while True:
    # for task in taskList:
    #     task.run()
    task1.run() 