"""
@file L6main.py

Main .py for Lab 6

Created on Tue Nov 17 14:02:19 2020

@author: Michelle Chandler
"""
import UI
import L6shares
import MotorDriver
import L6EncoderDriver
import DataGen_task
import ClosedLoop

import pyb


# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000);
tim4 = pyb.Timer(4, prescaler=0, period = 65535);

## define the pinB6
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)

## define the pinB7
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

print('setting up motor pins')
sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
IN1_pin = pyb.Pin(pyb.Pin.cpu.B4)
IN2_pin = pyb.Pin(pyb.Pin.cpu.B5)

EncDriver = L6EncoderDriver.Encoder_Driver(tim4, pinB6, pinB7, 65535) 
MotDriver = MotorDriver.Motor_Driver(sleep, IN1_pin, IN2_pin, tim) 
Loop = ClosedLoop.Closed_Loop()

task1 = DataGen_task.DataGen_Task(.02, EncDriver, Loop, MotDriver) 
task0 = UI.TaskUser(0, 1_000, dbg=False)

# taskList = [task0,
#             task1] 

while True:
#     for task in taskList:
#         task.run()
    task1.run()

 